<?php

namespace Tests\Feature;

use App\Models\Configuration\Roles;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class RolesControllerTest extends TestCase
{

    use DatabaseTransactions;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    // public function testRolesInfor()
    // {
    //     Método não existente no controller
    // }

    public function testRolesIndex()
    {
        $user = User::find(1);
        $response = $this->actingAs($user)->get(route('Panel.Roles.index'));
        $response->assertSuccessful();
    }

    // public function testRolesShow()
    // {
    //     View não existe
    // }

    public function testRolesCreate()
    {
        $user = User::find(1);
        $response = $this->actingAs($user)->get(route('Panel.Roles.create'));
        $response->assertSuccessful();
    }

    public function testRolesEdit()
    {
        $user = User::find(1);
        $roleEdit = Roles::find(1);
        $response = $this->actingAs($user)->get(route('Panel.Roles.edit', ['id' => $roleEdit->id]));
        $response->assertSuccessful();
    }

    public function testRolesStore()
    {
        $user = User::find(1);
        $rolestore = [
            'name'      => 'name',
            'label'     => 'label',
            'system'    => 1,
        ];
        $response = $this->actingAs($user)->post(route('Panel.Roles.store', $rolestore));
        $response->assertSessionHas('success');
        $this->assertDatabaseHas('Roles', ['name' => $rolestore['name']]);
    }

    public function testRolesUpdate()
    {
        $user = User::find(1);
        $roleUpdate = $this->createRoleTest();
        $roleUpdate->name = 'nameTest';
        $response = $this->actingAs($user)->post(route('Panel.Roles.update', $roleUpdate->toArray()));
        $response->assertSessionHas('success');
        $this->assertDatabaseHas('Roles', ['name' => $roleUpdate['name']]);
    }

    public function testRolesDelete()
    {
        $user = User::find(1);
        $roleUpdate = $this->createRoleTest();
        $response = $this->actingAs($user)->get(route('Panel.Roles.delete', $roleUpdate->toArray()));
        $response->assertSessionHas('success');
        $this->assertSoftDeleted('Roles', ['id' => $roleUpdate->id]);
    }

    // // public function testRolesTrashed()
    // // {
    // //      Método não existente no controller
    // // }

    public function testRolesRestore()
    {
        $user = User::find(1);
        $roleUpdate = $this->createRoleTest();
        $roleUpdate->delete();
        $response = $this->actingAs($user)->get(route('Panel.Roles.restore', $roleUpdate->toArray()));
        $response->assertSessionHas('success');
        $this->assertDatabaseHas('Roles', [
            'id' => $roleUpdate->id,
            'deleted_at' => null
        ]);
    }

    public function createRoleTest(){
        $roleCreate = [
            'name'      => 'name',
            'label'     => 'label',
        ];
        return Roles::create($roleCreate);
    }
}
