<?php

namespace Tests\Feature;

use App\Models\Configuration\Errors;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class ErrorsControllerTest extends TestCase
{

    use DatabaseTransactions;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    // public function testErrorsInfor()
    // {
    //     Método não existente no controller
    // }

    public function testErrorsFixed()
    {
        $user = User::find(1);
        $response = $this->actingAs($user)->get(route('Panel.Errors.fixed'));
        $response->assertSuccessful();
    }

    public function testErrorsIndex()
    {
        $user = User::find(1);
        $response = $this->actingAs($user)->get(route('Panel.Errors.index'));
        $response->assertSuccessful();
    }

    // public function testErrorsShow()
    // {
    //     View não existe
    // }

    public function testErrorsCreate()
    {
        $user = User::find(1);
        $response = $this->actingAs($user)->get(route('Panel.Errors.create'));
        $response->assertSuccessful();
    }

    public function testErrorsEdit()
    {
        $user = User::find(1);
        $errorEdit = $this->createErrorTest();
        $response = $this->actingAs($user)->get(route('Panel.Errors.edit', ['id' => $errorEdit->id]));
        $response->assertSuccessful();
    }

    public function testErrorsStore()
    {
        $user = User::find(1);
        $errorStore = [
            'user_id'   => 1,
            'title'     => 'titleTest',
            'code'      => '404',
            'content'   => 'ContentTest',
            'status'    => 1,
        ];
        $response = $this->actingAs($user)->post(route('Panel.Errors.store', $errorStore));
        $response->assertSessionHas('success');
        $this->assertDatabaseHas('Errors', ['title' => $errorStore['title']]);
    }

    public function testErrorsUpdate()
    {
        $user = User::find(1);
        $errorUpdate = $this->createErrorTest();
        $errorUpdate->title = 'titleUpdate';
        $response = $this->actingAs($user)->post(route('Panel.Errors.update', $errorUpdate->toArray()));
        $response->assertSessionHas('success');
        $this->assertDatabaseHas('Errors', ['title' => $errorUpdate['title']]);
    }

    public function testErrorsDelete()
    {
        $user = User::find(1);
        $errorDelete = $this->createErrorTest();
        $response = $this->actingAs($user)->get(route('Panel.Errors.delete', $errorDelete->toArray()));
        $response->assertSessionHas('success');
        $this->assertSoftDeleted('Errors', ['id' => $errorDelete->id]);
    }

    // // // public function testErrorsTrashed()
    // // // {
    // // //      Método não existente no controller
    // // // }

    public function testErrorsRestore()
    {
        $user = User::find(1);
        $errorRestore = $this->createErrorTest();
        $errorRestore->delete();
        $response = $this->actingAs($user)->get(route('Panel.Errors.restore', $errorRestore->toArray()));
        $response->assertSessionHas('success');
        $this->assertDatabaseHas('Errors', [
            'id' => $errorRestore->id,
            'deleted_at' => null
        ]);
    }

    public function createErrorTest(){
        $errorCreate = [
            'user_id'   => 1,
            'title'     => 'titleTest',
            'code'      => '404',
            'content'   => 'ContentTest',
            'status'    => 1,
        ];
        return Errors::create($errorCreate);
    }
}
