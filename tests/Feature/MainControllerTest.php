<?php

namespace Tests\Feature;

use App\Models\Configuration\Inscribes;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class InscribesControllerTest extends TestCase
{

    use DatabaseTransactions;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    // public function testInscribesInfor()
    // {
    //     Método não existente no controller
    // }

    public function testInscribesIndex()
    {
        $user = User::find(1);
        $response = $this->actingAs($user)->get(route('Panel.Main.index'));
        $response->assertSuccessful();
    }

    // public function testInscribesShow()
    // {
    //     View não existe
    // }

    // public function testInscribesCreate()
    // {
    //     $user = User::find(1);
    //     $response = $this->actingAs($user)->get(route('Panel.Main.create'));
    //     $response->assertSuccessful();
    // }

    // public function testInscribesEdit()
    // {
    //     $user = User::find(1);
    //     $inscribeEdit = Inscribes::find(1);
    //     $response = $this->actingAs($user)->get(route('Panel.Main.edit', ['id' => $inscribeEdit->id]));
    //     $response->assertSuccessful();
    // }

    public function testInscribesStore()
    {
        $user = User::find(1);
        $Inscribestore = null;
        $response = $this->actingAs($user)->post(route('Home.Main.store', ['event_id' => 3]));
        $response->assertSessionHas('success');
        $response->assertSessionHas('boleto');
        $response->assertSessionHas('model');

        // $this->assertDatabaseHas('Inscribes', ['name' => $Inscribestore['name']]);
    }

    // public function testInscribesUpdate()
    // {
    //     $user = User::find(1);
    //     $inscribeUpdate = $this->createinscribeTest();
    //     $response = $this->actingAs($user)->post(route('Panel.Main.update', $inscribeUpdate->toArray()));
    //     $response->assertSessionHas('success');
    //     $this->assertDatabaseHas('Inscribes', ['name' => $inscribeUpdate['name']]);
    // }

    // public function testInscribesDelete()
    // {
    //     $user = User::find(1);
    //     $inscribeDelete = $this->createinscribeTest();
    //     $response = $this->actingAs($user)->get(route('Panel.Main.delete', $inscribeDelete->toArray()));
    //     $response->assertSessionHas('success');
    //     $this->assertSoftDeleted('Inscribes', ['id' => $inscribeDelete->id]);
    // }

    // // // // public function testInscribesTrashed()
    // // // // {
    // // // //      Método não existente no controller
    // // // // }

    // public function testInscribesRestore()
    // {
    //     $user = User::find(1);
    //     $inscribeDelete = $this->createinscribeTest();
    //     $inscribeDelete->delete();
    //     $response = $this->actingAs($user)->get(route('Panel.Main.restore', $inscribeDelete->toArray()));
    //     $response->assertSessionHas('success');
    //     $this->assertDatabaseHas('Inscribes', [
    //         'id' => $inscribeDelete->id,
    //         'deleted_at' => null
    //     ]);
    // }

    public function createInscribeTest(){
        $inscribeCreate = [
            'name'      => 'nameTest',
            'label'     => 'labelTest',
            'group'     => 'groupTest',
            'system'    => 1,
            'default'   => 1,
        ];
        return Inscribes::create($inscribeCreate);
    }
}
