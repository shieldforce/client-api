<?php

namespace Tests\Feature;

use App\Models\Configuration\Permissions;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class PermissionsControllerTest extends TestCase
{

    use DatabaseTransactions;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    // public function testPermissionsInfor()
    // {
    //     Método não existente no controller
    // }

    public function testPermissionsIndex()
    {
        $user = User::find(1);
        $response = $this->actingAs($user)->get(route('Panel.Permissions.index'));
        $response->assertSuccessful();
    }

    // public function testPermissionsShow()
    // {
    //     View não existe
    // }

    public function testPermissionsCreate()
    {
        $user = User::find(1);
        $response = $this->actingAs($user)->get(route('Panel.Permissions.create'));
        $response->assertSuccessful();
    }

    public function testPermissionsEdit()
    {
        $user = User::find(1);
        $permissionEdit = Permissions::find(1);
        $response = $this->actingAs($user)->get(route('Panel.Permissions.edit', ['id' => $permissionEdit->id]));
        $response->assertSuccessful();
    }

    public function testPermissionsStore()
    {
        $user = User::find(1);
        $permissionStore = $this->createPermissionTest();
        $response = $this->actingAs($user)->post(route('Panel.Permissions.store', $permissionStore->toArray()));
        $response->assertSessionHas('success');
        $this->assertDatabaseHas('Permissions', ['name' => $permissionStore['name']]);
    }

    public function testPermissionsUpdate()
    {
        $user = User::find(1);
        $permissionUpdate = $this->createPermissionTest();
        $response = $this->actingAs($user)->post(route('Panel.Permissions.update', $permissionUpdate->toArray()));
        $response->assertSessionHas('success');
        $this->assertDatabaseHas('Permissions', ['name' => $permissionUpdate['name']]);
    }

    public function testPermissionsDelete()
    {
        $user = User::find(1);
        $permissionDelete = $this->createPermissionTest();
        $response = $this->actingAs($user)->get(route('Panel.Permissions.delete', $permissionDelete->toArray()));
        $response->assertSessionHas('success');
        $this->assertSoftDeleted('Permissions', ['id' => $permissionDelete->id]);
    }

    // // // public function testPermissionsTrashed()
    // // // {
    // // //      Método não existente no controller
    // // // }

    public function testPermissionsRestore()
    {
        $user = User::find(1);
        $permissionDelete = $this->createPermissionTest();
        $permissionDelete->delete();
        $response = $this->actingAs($user)->get(route('Panel.Permissions.restore', $permissionDelete->toArray()));
        $response->assertSessionHas('success');
        $this->assertDatabaseHas('Permissions', [
            'id' => $permissionDelete->id,
            'deleted_at' => null
        ]);
    }

    public function createPermissionTest(){
        $roleCreate = [
            'name'      => 'nameTest',
            'label'     => 'labelTest',
            'group'     => 'groupTest',
            'system'    => 1,
            'default'   => 1,
        ];
        return Permissions::create($roleCreate);
    }
}
