<?php

Route::group(['namespace'=>'Home'], function (){

    //Main
    Route::get('/', 'Main\MainController@index')->name('Home.Main.index');
    Route::get('/evento/{id?}/{tag?}', 'Main\MainController@show')->name('Home.Main.show');
    Route::post('/evento/{id?}/{tag?}', 'Main\MainController@show')->name('Home.Main.show');
    Route::post('/store/', 'Main\MainController@store')->name('Home.Main.store');
    Route::get('/evento/search/{id?}/{tag?}', 'Main\MainController@search')->name('Home.Main.search');
    Route::post('/evento/search/{id?}/{tag?}', 'Main\MainController@search')->name('Home.Main.search');

    //Validações javascript
    Route::get('/validationEmailInscribes/{email?}/{event_id?}', 'Main\MainController@validationEmailInscribes')
        ->name('Home.Main.validationEmailInscribes');
    Route::get('/validationCPFInscribes/{cpf?}/{event_id?}', 'Main\MainController@validationCPFInscribes')
        ->name('Home.Main.validationCPFInscribes');
    Route::get('/validationNumberRegistrationInscribes/{number_registration?}/{event_id?}', 'Main\MainController@validationNumberRegistrationInscribes')
        ->name('Home.Main.validationNumberRegistrationInscribes');

});

Auth::routes();

View::composer('*', function($view)
{
    $view
        ->with('users_temporary', \App\User::orderBy('id', 'DESC')->whereHas('roles', function ($query){$query->where('name', 'Temporary');})->get())
        ->with('variable', 'content');
});

Route::group(['namespace'=>'Panel'], function (){

    //Rota Panel
    Route::get('/Panel/Main', 'Main\MainController@index')->name('Panel.Main.index');

    //Rota Errors
    Route::get('/Panel/Errors/fixed', 'Errors\ErrorsController@fixed')->name('Panel.Errors.fixed');
    Route::get('/Panel/Errors/index', 'Errors\ErrorsController@index')->name('Panel.Errors.index');
    Route::get('/Panel/Errors/show/{id?}', 'Errors\ErrorsController@show')->name('Panel.Errors.show');
    //------
    Route::get('/Panel/Errors/create', 'Errors\ErrorsController@create')->name('Panel.Errors.create');
    Route::get('/Panel/Errors/edit/{id?}', 'Errors\ErrorsController@edit')->name('Panel.Errors.edit');
    //------
    Route::post('/Panel/Errors/store', 'Errors\ErrorsController@store')->name('Panel.Errors.store');
    Route::post('/Panel/Errors/update', 'Errors\ErrorsController@update')->name('Panel.Errors.update');
    Route::get('Panel/Errors/delete', 'Errors\ErrorsController@delete')->name('Panel.Errors.delete');
    //------
    Route::get('Panel/Errors/trashed', 'Errors\ErrorsController@trashed')->name('Panel.Errors.trashed');
    Route::get('Panel/Errors/restore', 'Errors\ErrorsController@restore')->name('Panel.Errors.restore');

    //Users
    Route::get('/Panel/Users/index', 'Users\UsersController@index')->name('Panel.Users.index');
    //Route::get('/Panel/Users/show/{id?}', 'Users\UsersController@show')->name('Panel.Users.show');
    //------
    Route::get('/Panel/Users/create', 'Users\UsersController@create')->name('Panel.Users.create');
    Route::get('/Panel/Users/edit/{id?}', 'Users\UsersController@edit')->name('Panel.Users.edit');
    //------
    Route::post('/Panel/Users/store', 'Users\UsersController@store')->name('Panel.Users.store');
    Route::post('/Panel/Users/update', 'Users\UsersController@update')->name('Panel.Users.update');
    Route::get('Panel/Users/delete', 'Users\UsersController@delete')->name('Panel.Users.delete');
    //------
    // Route::get('Panel/Users/trashed', 'Users\UsersController@trashed')->name('Panel.Users.trashed');
    Route::get('Panel/Users/restore', 'Users\UsersController@restore')->name('Panel.Users.restore');

    //Roles
    Route::get('/Panel/Roles/infor', 'Roles\RolesController@infor')->name('Panel.Roles.infor');
    Route::get('/Panel/Roles/index', 'Roles\RolesController@index')->name('Panel.Roles.index');
    Route::get('/Panel/Roles/show/{id?}', 'Roles\RolesController@show')->name('Panel.Roles.show');
    //------
    Route::get('/Panel/Roles/create', 'Roles\RolesController@create')->name('Panel.Roles.create');
    Route::get('/Panel/Roles/edit/{id?}', 'Roles\RolesController@edit')->name('Panel.Roles.edit');
    //------
    Route::post('/Panel/Roles/store', 'Roles\RolesController@store')->name('Panel.Roles.store');
    Route::post('/Panel/Roles/update', 'Roles\RolesController@update')->name('Panel.Roles.update');
    Route::get('Panel/Roles/delete', 'Roles\RolesController@delete')->name('Panel.Roles.delete');
    //------
    Route::get('Panel/Roles/trashed', 'Roles\RolesController@trashed')->name('Panel.Roles.trashed');
    Route::get('Panel/Roles/restore', 'Roles\RolesController@restore')->name('Panel.Roles.restore');

    //Permissions
    Route::get('/Panel/Permissions/infor', 'Permissions\PermissionsController@infor')->name('Panel.Permissions.infor');
    Route::get('/Panel/Permissions/index', 'Permissions\PermissionsController@index')->name('Panel.Permissions.index');
    Route::get('/Panel/Permissions/show/{id?}', 'Permissions\PermissionsController@show')->name('Panel.Permissions.show');
    //------
    Route::get('/Panel/Permissions/create', 'Permissions\PermissionsController@create')->name('Panel.Permissions.create');
    Route::get('/Panel/Permissions/edit/{id?}', 'Permissions\PermissionsController@edit')->name('Panel.Permissions.edit');
    //------
    Route::post('/Panel/Permissions/store', 'Permissions\PermissionsController@store')->name('Panel.Permissions.store');
    Route::post('/Panel/Permissions', 'Permissions\PermissionsController@update')->name('Panel.Permissions.update');
    Route::get('Panel/Permissions/delete', 'Permissions\PermissionsController@delete')->name('Panel.Permissions.delete');
    //------
    Route::get('Panel/Permissions/trashed', 'Permissions\PermissionsController@trashed')->name('Panel.Permissions.trashed');
    Route::get('Panel/Permissions/restore', 'Permissions\PermissionsController@restore')->name('Panel.Permissions.restore');

    //CmsPages
    Route::get('/Panel/CmsPages/infor', 'CmsPages\CmsPagesController@infor')->name('Panel.CmsPages.infor');
    Route::get('/Panel/CmsPages/index', 'CmsPages\CmsPagesController@index')->name('Panel.CmsPages.index');
    Route::get('/Panel/CmsPages/show/{id?}', 'CmsPages\CmsPagesController@show')->name('Panel.CmsPages.show');
    //------
    Route::get('/Panel/CmsPages/create', 'CmsPages\CmsPagesController@create')->name('Panel.CmsPages.create');
    Route::get('/Panel/CmsPages/edit/{id?}', 'CmsPages\CmsPagesController@edit')->name('Panel.CmsPages.edit');
    //------
    Route::post('/Panel/CmsPages/store', 'CmsPages\CmsPagesController@store')->name('Panel.CmsPages.store');
    Route::post('/Panel/CmsPages/update', 'CmsPages\CmsPagesController@update')->name('Panel.CmsPages.update');
    Route::get('Panel/CmsPages/delete', 'CmsPages\CmsPagesController@delete')->name('Panel.CmsPages.delete');
    //------
    Route::get('Panel/CmsPages/trashed', 'CmsPages\CmsPagesController@trashed')->name('Panel.CmsPages.trashed');
    Route::get('Panel/CmsPages/restore', 'CmsPages\CmsPagesController@restore')->name('Panel.CmsPages.restore');

});
