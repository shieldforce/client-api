<?php

use App\Events\Users\UserRegister;
use App\Models\Configuration\CmsPages;
use App\Models\Configuration\Permissions;
use App\Models\Configuration\Roles;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    //------------------------------------------------------------------------------------------------------------------
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(PermissionsTableSeeder::class);
        $this->call(CmsPagesTableSeeder::class);
    }
}

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('users')->delete();
        $user0 = User::create([
            'id'                   =>1,
            'name'                 =>'Administrador',
            'email'                =>'admin@admin',
            'password'             =>bcrypt('cgs@020459'),
            'system'               =>1,
            'create_user_id'       =>1
        ]);
        $user1 = User::create([
            'id'                   =>2,
            'name'                 =>'Development',
            'email'                =>'dev@didatico',
            'password'             =>bcrypt('dev@020459'),
            'system'               =>1,
            'create_user_id'       =>1
        ]);
    }
}


class RolesTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('roles')->delete();
        //Criando os Admins
        $role0 = Roles::create([
            'id'                   => 1,
            'name'                 =>'Administrator',
            'label'                =>'Administrador Master',
            'system'               =>1,
            'group'                =>'Funções do Sistema',
            'create_user_id'       =>1,
        ]);
        //Criando as Funções de Temporary
        $role2 = Roles::create([
            'id'                   => 2,
            'name'                 =>'Temporary',
            'label'                =>'Usuários Temporários',
            'system'               =>1,
            'group'                =>'Funções do Sistema',
            'create_user_id'       =>1,
        ]);

        //Criando as Funções de Dev
        $role3 = Roles::create([
            'id'                   => 3,
            'name'                 =>'Development1',
            'label'                =>'Função Geral de Desenvolvimento',
            'system'               =>1,
            'group'                =>'Funções do Sistema',
            'create_user_id'       =>1,
        ]);
        //------------------------------------------------
        event(new UserRegister(User::find(1), 1));
        event(new UserRegister(User::find(2), 3));
    }
}
class PermissionsTableSeeder extends Seeder
{
    public function run()
    {
        //----------------------------------------------------
        DB::table('permissions')->delete();
        //----------------------------------------------------

        $permissionPainel = Permissions::create([
            'name'                 =>'Panel.Main.index',
            'label'                =>'Pode Visualizar o Panel do Sistema',
            'group'                =>'Administrador',
            'system'               =>1,
            'default'              =>1,
            'create_user_id'       =>1
        ]);
        $permissionPainelConfig = Permissions::create([
            'name'                 =>'Panel.Main.update',
            'label'                =>'Pode Configurar dados do Sistema',
            'group'                =>'Administrador',
            'system'               =>1,
            'create_user_id'       =>1
        ]);
        $permissionHome = Permissions::create([
            'name'                 =>'Home.Main.index',
            'label'                =>'Pode Visualizar o Home do Sistema',
            'group'                =>'Público',
            'system'               =>1,
            'create_user_id'       =>1
        ]);
        $permissionPainel->roles()->sync(Roles::where('name', '!=', 'Administrator')->where('name', '!=', 'Development1')->get());
        $permissionHome->roles()->sync(Roles::where('name', '!=', 'Administrator')->where('name', '!=', 'Development1')->get());
        //----------------------------------------------------
        $permissions =
            [
                //Users--------------------------------
                [
                    'ambient'     =>'Panel',
                    'name'        =>'Users',
                    'group'       =>'Usuários',
                ],
                //Roles--------------------------------
                [
                    'ambient'     =>'Panel',
                    'name'        =>'Roles',
                    'group'       =>'Funções',
                ],
                //Permissions--------------------------
                [
                    'ambient'     =>'Panel',
                    'name'        =>'Permissions',
                    'group'       =>'Permissões',
                ],
            ];
        //----------------------------------------------------
        foreach ($permissions as $p)
        {
            $permissionAll = Permissions::create([
                'name'                 =>$p['ambient'].'.'.$p['name'].'.all',
                'label'                =>"Pode ver a lista de todos ".$p['name'].' do sistema',
                'group'                =>$p['group'],
                'system'               =>1,
                'create_user_id'       =>1
            ]);
            $permissionShow1 = Permissions::create([
                'name'                 =>$p['ambient'].'.'.$p['name'].'.index',
                'label'                =>"Pode Acessar Lista ou item de ".$p['name'],
                'group'                =>$p['group'],
                'system'               =>1,
                'create_user_id'       =>1
            ]);
            $permissionShow2 = Permissions::create([
                'name'                 =>$p['ambient'].'.'.$p['name'].'.create',
                'label'                =>"Pode Visualizar a Tela de Cadastro ".$p['name'],
                'group'                =>$p['group'],
                'system'               =>1,
                'create_user_id'       =>1
            ]);
            $permissionShow3 = Permissions::create([
                'name'                 =>$p['ambient'].'.'.$p['name'].'.edit',
                'label'                =>"Pode Visualizar a Tela de Editar ".$p['name'],
                'group'                =>$p['group'],
                'system'               =>1,
                'create_user_id'       =>1
            ]);
            $permissionShow4 = Permissions::create([
                'name'                 =>$p['ambient'].'.'.$p['name'].'.infor',
                'label'                =>"Pode Visualizar Informações ".$p['name'],
                'group'                =>$p['group'],
                'system'               =>1,
                'create_user_id'       =>1
            ]);
            $permissionShow5 = Permissions::create([
                'name'                 =>$p['ambient'].'.'.$p['name'].'.show',
                'label'                =>"Pode Visualizar Item único de ".$p['name'],
                'group'                =>$p['group'],
                'system'               =>1,
                'create_user_id'       =>1
            ]);
            $permissionShow6 = Permissions::create([
                'name'                 =>$p['ambient'].'.'.$p['name'].'.store',
                'label'                =>"Pode Cadastrar ".$p['name'],
                'group'                =>$p['group'],
                'system'               =>1,
                'create_user_id'       =>1
            ]);
            $permissionShow7 = Permissions::create([
                'name'                 =>$p['ambient'].'.'.$p['name'].'.update',
                'label'                =>"Pode Editar ".$p['name'],
                'group'                =>$p['group'],
                'system'               =>1,
                'create_user_id'       =>1
            ]);
            $permissionShow8 = Permissions::create([
                'name'                 =>$p['ambient'].'.'.$p['name'].'.delete',
                'label'                =>"Pode Deletar ".$p['name'],
                'group'                =>$p['group'],
                'system'               =>1,
                'create_user_id'       =>1
            ]);
            $permissionShow9 = Permissions::create([
                'name'                 =>$p['ambient'].'.'.$p['name'].'.trashed',
                'label'                =>"Pode ver lista de deletados de ".$p['name'],
                'group'                =>$p['group'],
                'system'               =>1,
                'create_user_id'       =>1
            ]);
            $permissionShow10 = Permissions::create([
                'name'                 =>$p['ambient'].'.'.$p['name'].'.restore',
                'label'                =>"Pode restaurar deletados de ".$p['name'],
                'group'                =>$p['group'],
                'system'               =>1,
                'create_user_id'       =>1
            ]);
        }
        //----------------------------------------------------
    }
}


    class CmsPagesTableSeeder extends Seeder
    {
        public function run()
        {
            DB::table('cms_pages')->delete();
            $cms1 = CmsPages::create([
                'page_name'                 =>'Home Principal',
                'logo1'                     =>'default',
                'facebook'                  =>'facebook',
                'twitter'                   =>'twitter',
                'session_1_img_background'  =>'default',
                'session_1_title'           =>'Primeiro Título',
                'session_1_subtitle'        =>'Usu habeo equidem sanctus no ex melius labitur conceptam eos',
                'session_2_img_right'       =>'default',
                'session_2_title'           =>'Vim ridens eleifend referrentur eu',
                'session_2_content'         =>'Ex graeco nostrud theophrastus nam, cum tibique reprimique ad. Mea omittam electram te, eu cum fastidii sapientem delicatissimi.',
                'session_3_title'           =>'MEET WILIO TEAM',
                'session_3_subtitle'        =>'Usu habeo equidem sanctus no. Suas summo id sed, erat erant oporteat cu pri. In eum omnes molestie. Sed ad debet scaevola, ne mel lorem legendos.',
            ]);
        }
    }
