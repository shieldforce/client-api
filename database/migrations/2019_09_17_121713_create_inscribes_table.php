<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInscribesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inscribes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('registration_id')->nullable();
            $table->unsignedBigInteger('person_id')->nullable();
            $table->unsignedBigInteger('event_id');
            $table->integer('status')->nullable();
            $table->boolean('kit')->nullable()->default(0);

             //----------------------------------------------------------------
             $table->unsignedBigInteger('create_user_id')->nullable();
             $table->foreign('create_user_id')
                 ->references('id')
                 ->on('users')
                 ->onDelete('cascade');
             $table->unsignedBigInteger('update_user_id')->nullable();
             $table->foreign('update_user_id')
                 ->references('id')
                 ->on('users')
                 ->onDelete('cascade');
             $table->unsignedBigInteger('delete_user_id')->nullable();
             $table->foreign('delete_user_id')
                 ->references('id')
                 ->on('users')
                 ->onDelete('cascade');
             //----------------------------------------------------------------

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inscribes');
    }
}
