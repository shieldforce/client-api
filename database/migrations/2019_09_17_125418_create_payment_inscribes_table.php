<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentInscribesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_inscribes', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('inscribe_id')->nullable();
            $table->foreign('inscribe_id')
                ->references('id')
                ->on('inscribes')
                ->onDelete('cascade');

            $table->string('type');
            $table->string('name');
            $table->decimal('value', 10,2);
            $table->date('inscribe_date');
            $table->date('payment_date')->nullable();
            $table->date('expiry_date')->nullable();
            $table->string('link_boleto')->nullable();
            $table->string('code');
            $table->integer('status');
            $table->integer('email_confirmed');

            //----------------------------------------------------------------
            $table->unsignedBigInteger('create_user_id')->nullable();
            $table->foreign('create_user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            $table->unsignedBigInteger('update_user_id')->nullable();
            $table->foreign('update_user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            $table->unsignedBigInteger('delete_user_id')->nullable();
            $table->foreign('delete_user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            //----------------------------------------------------------------


            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_inscribes');
    }
}
