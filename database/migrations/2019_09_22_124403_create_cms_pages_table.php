<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCmsPagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_pages', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('page_name')->nullable();

            $table->string('logo1')->nullable();
            $table->string('logo2')->nullable();
            $table->string('logo3')->nullable();

            $table->string('facebook')->nullable();
            $table->string('twitter')->nullable();
            $table->string('google')->nullable();
            $table->string('linkedin')->nullable();
            $table->string('youtube')->nullable();
            $table->string('instagram')->nullable();

            $table->string('session_1_img_background')->nullable();
            $table->string('session_1_title')->nullable();
            $table->string('session_1_subtitle')->nullable();

            $table->string('session_2_img_right')->nullable();
            $table->string('session_2_title')->nullable();
            $table->longText('session_2_content')->nullable();

            $table->string('session_3_title')->nullable();
            $table->string('session_3_subtitle')->nullable();

            //----------------------------------------------------------------
            $table->unsignedBigInteger('create_user_id')->nullable();
            $table->foreign('create_user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            $table->unsignedBigInteger('update_user_id')->nullable();
            $table->foreign('update_user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            $table->unsignedBigInteger('delete_user_id')->nullable();
            $table->foreign('delete_user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            //----------------------------------------------------------------

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_pages');
    }
}
