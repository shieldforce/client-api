<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpeechesInscribesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('speeches_inscribes', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('inscribe_id');
            $table->foreign('inscribe_id')
                ->references('id')
                ->on('inscribes')
                ->onDelete('cascade');

            $table->unsignedBigInteger('speeche_id');
            $table->integer('status');
            $table->string('hash');

            //----------------------------------------------------------------
            $table->unsignedBigInteger('create_user_id')->nullable();
            $table->foreign('create_user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            $table->unsignedBigInteger('update_user_id')->nullable();
            $table->foreign('update_user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            $table->unsignedBigInteger('delete_user_id')->nullable();
            $table->foreign('delete_user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            //----------------------------------------------------------------

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('speeches_inscribes');
    }
}
