<?php

namespace App\Models\Configuration;

use App\Observers\Logs\InterceptCrudObserver;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Roles extends Model
{

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'roles';

    protected $fillable = [
        'name',
        'label',
        'system',
    ];

    public static function boot()
    {
        parent::boot();

        static::observe(new InterceptCrudObserver);
    }

    //------------------------------------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------------------------------------------
    public function permissions()
    {
        return $this->belongsToMany(\App\Models\Configuration\Permissions::class, 'permission_role', 'role_id', 'permission_id')->withoutGlobalScopes();
    }

    public function users()
    {
        return $this->belongsToMany(\App\User::class, 'role_user', 'role_id', 'user_id')->withoutGlobalScopes();
    }

    //---------------------------------------------------------------------------------------------
    public function create_user()
    {
        return $this->hasOne(\App\User::class, 'id', 'create_user_id');
    }
    public function update_user()
    {
        return $this->hasOne(\App\User::class, 'id', 'update_user_id');
    }
    public function delete_user()
    {
        return $this->hasOne(\App\User::class, 'id', 'delete_user_id');
    }
    //---------------------------------------------------------------------------------------------

}
