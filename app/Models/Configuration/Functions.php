<?php


namespace App\Models\Configuration;


class Functions
{

    public static function importaCSV($file, $delimitador){
        $linhas = array();

        try{
            // Abrir arquivo para leitura
            $f = fopen($file, 'r');
            if ($f) {

                // Enquanto nao terminar o arquivo
                while (!feof($f)) {

                    // Ler uma linha do arquivo
                    $linha = fgetcsv($f, 0, $delimitador);
                    if (!$linha) {
                        continue;
                    }

                    $linha = array_map("utf8_encode", $linha);
                    array_push($linhas, $linha);
                }
                fclose($f);
            }
            return $linhas;
        }
        catch (\Exception $e)
        {
            if(env('APP_DEBUG')=='true')
            {
                dd($e->getMessage());
            }
            return "Erro na Importação";
        }
    }

    public static function removeMaskPhone($phone)
    {
        $phone = str_replace(['(', ')', '-'], '', $phone);
        $phone = array(
            'codeArea' => substr($phone, 0, 2),
            'phone'    => substr($phone, 2, 11)
        );
        return $phone;
    }

    public static function setMaskPhone($numberPhone, $codeArea = null)
    {
        if (isset($codeArea)) {
            if (strlen($numberPhone) == 9) {
                return '(' . $codeArea . ')' . substr($numberPhone,0, 5) . '-' . substr($numberPhone,5, 9);
            } else {
                return '(' . $codeArea . ')' . substr($numberPhone,0, 4) . '-' . substr($numberPhone,4, 8);
            }
        } else {
            if (strlen($numberPhone) == 11) {
                return '(' . substr($numberPhone,0, 2) . ')' . substr($numberPhone,2, 7) . '-' . substr(7, 11);
            } else {
                return '(' . substr($numberPhone,0, 2) . ')' . substr($numberPhone,2, 6) . '-' . substr(6, 10);
            }
        }
    }

    public static function removeMaskCpf($cpf)
    {
        return str_replace(['.', '-'], '', $cpf);
    }

    public static function formatDateBD($date)
    {
        $date = explode('/', $date);
        $date = date('Y-m-d', strtotime($date[2].'-'.$date[1].'-'.$date[0]));
        return $date;
    }

    public static function formatDateView($date)
    {
        $date = explode('-', $date);
        $date = date('d/m/Y', strtotime($date[2].'-'.$date[1].'-'.$date[0]));
        return $date;
    }
}