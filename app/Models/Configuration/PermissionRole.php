<?php

namespace App\Models\Configuration;

use App\Observers\Logs\InterceptCrudObserver;
use Illuminate\Database\Eloquent\Model;

class PermissionRole extends Model
{

    protected $table = 'permission_role';

    protected $fillable = [
        'permission_id',
        'role_id',
    ];

    public static function boot()
    {
        parent::boot();

        static::observe(new InterceptCrudObserver);
    }


    /*
     *
     * Relacionamentos
     *
     */

}
