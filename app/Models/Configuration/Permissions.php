<?php

namespace App\Models\Configuration;

use App\Observers\Logs\InterceptCrudObserver;
use App\Scopes\ListAll\ListAllScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Permissions extends Model
{

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'permissions';

    protected $fillable = [
        'name',
        'label',
        'group',
        'system',
        'default',
    ];

    public static function boot()
    {
        parent::boot();

        static::observe(new InterceptCrudObserver);
    }

    /*
     *
     * Relacionamentos
     *
     */
    public function roles()
    {
        return $this->belongsToMany(\App\Models\Configuration\Roles::class, 'permission_role', 'permission_id', 'role_id')->withoutGlobalScopes();
    }

    //---------------------------------------------------------------------------------------------
    public function create_user()
    {
        return $this->hasOne(\App\User::class, 'id', 'create_user_id');
    }
    public function update_user()
    {
        return $this->hasOne(\App\User::class, 'id', 'update_user_id');
    }
    public function delete_user()
    {
        return $this->hasOne(\App\User::class, 'id', 'delete_user_id');
    }
    //---------------------------------------------------------------------------------------------

}
