<?php

namespace App\Models\Configuration;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Errors extends Model
{

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'errors';

    protected $fillable = [
        'user_id',
        'title',
        'code',
        'content',
        'status',
    ];

    public static function boot()
    {
        parent::boot();
    }


    /*
     *
     * Relacionamentos
     *
     */
    public function user()
    {
        return $this->hasOne(\App\User::class, 'id', 'user_id');
    }

}
