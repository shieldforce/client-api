<?php

namespace App\Models\Configuration;

use App\Observers\Logs\InterceptCrudObserver;
use Illuminate\Database\Eloquent\Model;

class RoleUser extends Model
{

    protected $table = 'role_user';

    protected $fillable = [
        'role_id',
        'user_id',
    ];

    public static function boot()
    {
        parent::boot();

        static::observe(new InterceptCrudObserver);
    }


    /*
     *
     * Relacionamentos
     *
     */

}
