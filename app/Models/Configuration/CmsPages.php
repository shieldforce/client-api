<?php

namespace App\Models\Configuration;

use App\Observers\Logs\InterceptCrudObserver;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CmsPages extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'cms_pages';

    protected $fillable = [
        'page_name',
        'logo1',
        'logo2',
        'logo3',
        'facebook',
        'twitter',
        'google',
        'linkedin',
        'youtube',
        'instagram',
        'session_1_img_background',
        'session_1_title',
        'session_1_subtitle',
        'session_2_img_right',
        'session_2_title',
        'session_2_content',
        'session_3_title',
        'session_3_subtitle',
    ];

    public static function boot()
    {
        parent::boot();

        static::observe(new InterceptCrudObserver);
    }

    /*
     *
     * Relacionamentos
     *
     */
}
