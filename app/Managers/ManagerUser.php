<?php


namespace App\Managers;


use App\Models\Configuration\Configurations;
use App\Models\Configuration\Permissions;
use Illuminate\Http\Request;

class ManagerUser
{
    protected                     $request;
    protected                     $configurations;
    protected                     $routeArray;

    public function __construct
    (
        Request                   $request,
        Configurations             $configurations
    )
    {
        $this->request          = $request;
        $this->configurations   = $configurations;
        $this->routeArray       = $this->configurations->getRouteName($request);
    }

    public function getUserIdentify()
    {
        return auth()->user();
    }

    public function getUserListAllIdentify()
    {
        foreach (auth()->user()->roles[0]->permissions as $listAll)
        {
            $route = $this->routeArray;
            if($listAll->name=="$route[0].$route[1].all")
            {
               return true;
            }
        }
        return false;
    }
}
