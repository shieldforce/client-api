<?php


namespace App\Observers\Logs;


use App\Managers\ManagerUser;
use Illuminate\Database\Eloquent\Model;

class InterceptCrudObserver
{
    public function creating(Model $model)
    {
        if(auth()->user())
            $user = app(ManagerUser::class)->getUserIdentify();
            $model->setAttribute('create_user_id', $user->id ?? null);
    }
    public function saving(Model $model)
    {
        if(auth()->user())
            $user = app(ManagerUser::class)->getUserIdentify();
            $model->update_user_id =  $user->id ?? null;
    }
    public function deleted(Model $model)
    {
        if(auth()->user())
            $user = app(ManagerUser::class)->getUserIdentify();
            $model->delete_user_id = $user->id ?? null;
            $model->save();
    }
}
