<?php

    namespace App\APIIntranet;

    use GuzzleHttp\Client;
    use Illuminate\Http\Request;

    class Functions
    {
        //Variáveis Globais
        protected                     $request;

        //Funções Padrões
        public function __construct
        (
            Request                   $request
        )
        {
            $this->request          = $request;
        }

        public function credentials()
        {
            return
                [
                    'email'         => env('MAIL_USERNAME'),
                    'password'      => env('MAIL_PASSWORD'),
                    'app_name'      => env('APP_NAME'),
                ];
        }

        public function login()
        {
            try
            {
                $url = url(env('PATH_API_EUNIG') . 'login');
                $headers =
                    [
                        'Host'       => env('PATH_HOST_EUNIG'),
                        'Accept'     => 'application/json'
                    ];
                $http = new Client;
                $response = $http->post($url, [
                    'headers' => $headers,
                    'form_params' => [
                        'email'         => $this->credentials()['email'],
                        'password'      => $this->credentials()['password'],
                        'app_name'      => $this->credentials()['app_name'],
                    ],
                ]);
                return json_decode((string) $response->getBody()->getContents(), true);
            }
            catch(\Exception $e)
            {
                if(env('APP_DEBUG')==true)
                {
                    dd($e->getMessage());
                }
                return response()->json(['error'=>$e->getMessage()], $e->getCode(),
                    ['Content-type'=>'application/json;charset=utf-8'],JSON_UNESCAPED_UNICODE);
            }
        }

        public function consume($method_request, $crud, $method, $request=null, $var1=null, $var2=null)
        {
            try
            {
                if($method_request=='post')
                {
                    $url = url(env('PATH_API_EUNIG') . $crud . "/" . $method);
                }
                if(($method_request=='get' || $method_request=='put' || $method_request=='delete'))
                {
                    $url = url(env('PATH_API_EUNIG') . $crud . "/" . $method . ($var1!=null ?  "/$var1" : "") . ($var2!=null ?  "/$var2" : ""));
                }
                $headers =
                    [
                        'Accept'          => 'application/json',
                        'Authorization'   => "Bearer " . $this->login()['data']['access_token'],
                        'content-type'    => 'application/json',
                    ];
                $http = new Client;
                $request_method = $method_request;
                $response = $http->$request_method($url, [
                    'headers'            => $headers,
                    'json'               => $request,
                ]);
                return json_decode((string) $response->getBody()->getContents(), true);
            }
            catch(\Exception $e)
            {
                if(env('APP_DEBUG')==true)
                {
                    dd($e->getMessage());
                }
                return response()->json(['error'=>$e->getMessage()], $e->getCode(),
                    ['Content-type'=>'application/json;charset=utf-8'],JSON_UNESCAPED_UNICODE);
            }
        }

        public function examples(Request $request)
        {
            /*
            //List all itens
            $consume = $this->consume("get", "Events", "index");

            //Show Item
            $consume = $this->consume("get", "Events", "show", null, 1);

            //Store All request
            $consume = $this->consume("post", "Events", "store", $request->all());

            //Store in array
            $data =
                [
                    'institution_id'      =>1,
                    'name'                =>'teste',
                    'code'                =>'2512',
                    'start'               =>'2019-10-01',
                    'end'                 =>'2019-10-01',
                    'start_inscribes'     =>'2019-10-01',
                    'end_inscribes'       =>'2019-10-01',
                    'status'              =>1,
                    'price'               =>30.00,
                    'price_two'           =>0.00,
                    'tag'                 =>'teste',
                    'type_event_id'       =>1,
                    'url'                 =>'http://teste',
                ];
            $consume = $this->consume("post","Events","store", $data, null);


            //Update All request
            $consume = $this->consume("put", "Events", "update", $request->all());

            //Update in array
            $data =
                [
                    'id'                  =>1,
                    'institution_id'      =>1,
                    'name'                =>'teste',
                    'code'                =>'2512',
                    'start'               =>'2019-10-01',
                    'end'                 =>'2019-10-01',
                    'start_inscribes'     =>'2019-10-01',
                    'end_inscribes'       =>'2019-10-01',
                    'status'              =>1,
                    'price'               =>30.00,
                    'price_two'           =>0.00,
                    'tag'                 =>'teste',
                    'type_event_id'       =>1,
                    'url'                 =>'http://teste',
                ];
            $consume = $this->consume("put","Events","update", $data, null);

            //Delete
            $consume = $this->consume("delete","Events","delete", null, 1);

            //Restore
            $consume = $this->consume("get","Events","restore", null, 1);
            */
        }
    }
