<?php

    namespace App\APIIntranet\Models;

    use App\APIIntranet\Functions;
    use App\Models\Configuration\CmsPages;
    use App\Models\Configuration\Configurations;
    use Illuminate\Http\Request;

    class ModelBase
    {
        //Variáveis Globais
        protected                     $request;
        protected                     $apiIntranet;
        protected                     $routeArray;

        //Funções Padrões
        public function __construct
        (
            Request                   $request,
            Configurations            $configurations,
            Functions                 $apiIntranet

        )
        {
            $this->request          = $request;
            $this->configurations   = $configurations;
            $this->routeArray       = $this->configurations->getRouteName($request);
            $this->apiIntranet      = $apiIntranet;
            $this->auth_eunig       = $this->apiIntranet->login();
        }

        public function index(Request $request, $crud)
        {
            $consume = $this->apiIntranet->consume("get", "$crud", "index", $request->all(), null, null);
            return $consume;
        }

        public function show(Request $request, $crud, $var1)
        {
            $consume = $this->apiIntranet->consume("get", "$crud", "show", $request->all(), $var1, null);
            return $consume;
        }

        public function create(Request $request, $crud)
        {
            //===========================================
        }

        public function store(Request $request, $crud)
        {
            $consume = $this->apiIntranet->consume("post","$crud","store", $request->all(), null, null);
            return $consume;
        }

        public function edit(Request $request, $crud)
        {
            //============================================
        }

        public function update(Request $request, $crud)
        {
            $consume = $this->apiIntranet->consume("put","$crud","update", $request->all(), null, null);
            return $consume;
        }

        public function delete(Request $request, $crud, $var1)
        {
            $consume = $this->apiIntranet->consume("delete","$crud","delete", $request->all(), $var1, null);
            return $consume;
        }

        public function restore(Request $request, $crud, $var1)
        {
            $consume = $this->apiIntranet->consume("get","$crud","restore", $request->all(), $var1, null);
            return $consume;
        }

        public function errors($model)
        {
            if($model['status']=='unauthorized' || $model['status']=='error')
            {
                return
                    [
                        'route'       =>
                            [
                                'ambient'    => 'Home',
                                'crud'       => 'Errors',
                                'function'   => 'show',
                            ],
                        'compact'     =>
                            [
                                'title'      => "Error: ".$model['code'],
                                'route'      => $this->routeArray,
                                'crudName'   => "Error: ".$model['code'],
                                'model'      => null,
                                'cms_page'   => CmsPages::find(1),
                                'msgError'   => $model['msg']
                            ],
                        'request'            => null,
                    ];
            }
            else
            {
                return false;
            }
        }
    }
