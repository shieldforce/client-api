<?php


namespace App\Response;


class Response
{
    public function return($status, $method, $request=null, $data=null)
    {
        $list_codes =
            [
                'success'=>
                    [
                        'index'=>
                            [
                                'code'=>1000,
                                'msg'=>'Sucesso em visualizar lista!',
                            ],
                        'show'=>
                            [
                                'code'=>1001,
                                'msg'=>'Sucesso em visualizar item!',
                            ],
                        'store'=>
                            [
                                'code'=>1002,
                                'msg'=>'Sucesso em criar item solicitado!',
                            ],
                        'update'=>
                            [
                                'code'=>1003,
                                'msg'=>'Sucesso em editar item solicitado!',
                            ],
                        'delete'=>
                            [
                                'code'=>1004,
                                'msg'=>'Sucesso em excluir item solicitado!',
                            ],
                        'create'=>
                            [
                                'code'=>1007,
                                'msg'=>'Sucesso em visualizar tela de cadastro!',
                            ],
                        'edit'=>
                            [
                                'code'=>1008,
                                'msg'=>'Sucesso em visualizar tela de edição!',
                            ],
                        'fixed'=>
                            [
                                'code'=>1009,
                                'msg'=>'Sucesso em visualizar tela de erro!',
                            ],
                        'restore'=>
                            [
                                'code'=>1010,
                                'msg'=>'Sucesso em restaurar item!',
                            ],
                        'validationNumberRegistration'=>
                            [
                                'code'=>1011,
                                'msg'=>'Sucesso em verificar matrícula!',
                            ],
                        'search'=>
                            [
                                'code'=>1012,
                                'msg'=>'Sucesso em ver rota de consulta!',
                            ],
                    ],
                'error'=>
                    [
                        'index'=>
                            [
                                'code'=>2000,
                                'msg'=>'Não foi possível visualizar lista!',
                            ],
                        'show'=>
                            [
                                'code'=>2001,
                                'msg'=>'Não foi possível visualizar item!',
                            ],
                        'store'=>
                            [
                                'code'=>2002,
                                'msg'=>'Não foi possível criar item solicitado!',
                            ],
                        'update'=>
                            [
                                'code'=>2003,
                                'msg'=>'Não foi possível editar item solicitado!',
                            ],
                        'delete'=>
                            [
                                'code'=>2004,
                                'msg'=>'Não foi possível excluir item solicitado!',
                            ],
                        'create'=>
                            [
                                'code'=>2007,
                                'msg'=>'Não foi possível visualizar tela de cadastro!',
                            ],
                        'edit'=>
                            [
                                'code'=>2008,
                                'msg'=>'Não foi possível visualizar tela de edição!',
                            ],
                        'fixed'=>
                            [
                                'code'=>2009,
                                'msg'=>'Não foi possível visualizar tela de erro!',
                            ],
                        'restore'=>
                            [
                                'code'=>2010,
                                'msg'=>'Não foi possível restaurar item!',
                            ],
                        'validationNumberRegistration'=>
                            [
                                'code'=>2011,
                                'msg'=>'Não foi possível verificar matrícula!',
                            ],
                        'search'=>
                            [
                                'code'=>2012,
                                'msg'=>'Error em ver rota de consulta!',
                            ],
                    ]
            ];
        //--------------------------------------------------------------------------------------------------------------
        return
            [
                'msg'         =>$list_codes[$status][$method]['msg'],
                'code'        =>$list_codes[$status][$method]['code'],
                'status'      =>$status,
                'request'     =>$request,
                'data'        =>$data,
            ];

    }
}
