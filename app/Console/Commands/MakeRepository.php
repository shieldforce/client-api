<?php

namespace App\Console\Commands;

use Illuminate\Console\GeneratorCommand;
use Symfony\Component\Console\Input\InputArgument;

class MakeRepository extends GeneratorCommand
{
    /**
     * O nome e a assinatura do comando do console.
     *
     * @var string
     */
    protected $name = 'make:repository-crud';

    /**
     * A descrição do comando do console.
     *
     * @var string
     */
    protected $description = 'Create a new repository class';

    /**
     * O tipo de classe sendo gerada.
     *
     * @var string
     */
    protected $type = 'Repository';

    /**
     * Substitui o nome da classe para o stub fornecido.
     *
     * @param  string  $stub
     * @param  string  $name
     * @return string
     */
    protected function replaceClass($stub, $name)
    {
        $name = $this->argument('name');
        if (strpos($name, '/') != false)
        {
            $array_path         = explode('/', $name);
            $className          = array_pop($array_path);
            $nameModel          = str_replace('Repository', '', $className);
        }
        else
        {
            $className = $name;
            $nameModel = $name;
        }
        $stub = parent::replaceClass($stub, $className);
        return str_replace(['ClassName', 'NameModel'], [$className, $nameModel], $stub);
    }
    /**
     * Obtém o arquivo stub para o gerador.
     *
     * @return string
     */
    protected function getStub()
    {
        return  app_path() . '/Console/Commands/Stubs/Repository/make-repository.stub';
    }
    /**
     * Obtém o namespace padrão para a classe.
     *
     * @param  string  $rootNamespace
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace . '\Repositories';
    }

    /**
     * Obtém os argumentos do comando do console.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['name', InputArgument::REQUIRED, 'The name of the repository.'],
        ];
    }
}
