<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class MakeCrud extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:crud {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command sequence test';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = $this->argument('name');
        $this->call('make:model-crud',['name' => $this->fixPath($name,'Model')]);
        $this->call('make:controller-crud',['name' => $this->fixPath($name,'Controller')]);
        $this->call('make:service-crud',['name' => $this->fixPath($name,'Service')]);
        $this->call('make:validator-crud',['name' => $this->fixPath($name,'Validator')]);
        $this->call('make:repository-crud',['name' => $this->fixPath($name,'Repository')]);
    }

    //Função para corrigir os pathnames para a criação das pastas em seus devido lugares
    private function fixPath($path, $prefix)
    {

        if (strpos($path, '/') != false)
        {
            $path = explode('/', $path);
            $last_name = array_pop($path);
            $path = implode('/', $path) . '/';
        }
        else
        {
            $last_name = $path;
            $path = '';
        }

        switch ($prefix) {
            case 'Model':
                $path = $last_name;
                break;
            case 'Controller':
                $path .= $last_name . $prefix;
                break;
            default:
                $path .= $prefix . $last_name;
        }

        return $path;
    }
}
