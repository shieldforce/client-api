<?php

namespace App\Console\Commands;

use Illuminate\Console\GeneratorCommand;
use Symfony\Component\Console\Input\InputArgument;

class MakeValidator extends GeneratorCommand
{
    /**
     * O nome e a assinatura do comando do console.
     *
     * @var string
     */
    protected $name = 'make:validator-crud';

    /**
     * A descrição do comando do console.
     *
     * @var string
     */
    protected $description = 'Create a new validator class';

    /**
     * O tipo de classe sendo gerada.
     *
     * @var string
     */
    protected $type = 'Validator';

    /**
     * Substitui o nome da classe para o stub fornecido.
     *
     * @param  string  $stub
     * @param  string  $name
     * @return string
     */
    protected function replaceClass($stub, $name)
    {
        $name = $this->argument('name');
        if (strpos($name, '/') != false)
        {
            $array_path         = explode('/', $name);
            $className          = array_pop($array_path);
            $nameModel          = str_replace('Validator', '', $className);
        }
        else
        {
            $className = $name;
            $nameModel = $name;
        }
        $stub = parent::replaceClass($stub, $className);
        return str_replace(['ClassName', 'NameModel'], [$className, $nameModel], $stub);
    }
    /**
     * Obtém o arquivo stub para o gerador.
     *
     * @return string
     */
    protected function getStub()
    {
        return  app_path() . '/Console/Commands/Stubs/Validator/make-validator.stub';
    }
    /**
     * Obtém o namespace padrão para a classe.
     *
     * @param  string  $rootNamespace
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace . '\Validations';
    }

    /**
     * Obtém os argumentos do comando do console.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['name', InputArgument::REQUIRED, 'The name of the validator.'],
        ];
    }
}
