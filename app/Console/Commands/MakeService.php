<?php

namespace App\Console\Commands;

use Illuminate\Console\GeneratorCommand;
use Symfony\Component\Console\Input\InputArgument;

class MakeService extends GeneratorCommand
{
    /**
     * O nome e a assinatura do comando do console.
     *
     * @var string
     */
    protected $name = 'make:service-crud';

    /**
     * A descrição do comando do console.
     *
     * @var string
     */
    protected $description = 'Create a new Service CRUD class';

    /**
     * O tipo de classe sendo gerada.
     *
     * @var string
     */
    protected $type = 'Service';

    /**
     * Substitui o nome da classe para o stub fornecido.
     *
     * @param  string  $stub
     * @param  string  $name
     * @return string
     */
    protected function replaceClass($stub, $name)
    {
        $name = $this->argument('name');
        if (strpos($name, '/') != false)
        {
            $array_path              = explode('/', $name);
            $className               = array_pop($array_path);
            $dependenceRepository    = str_replace('/', DIRECTORY_SEPARATOR, $name);
            $dependenceRepository    = str_replace('Service', 'Repository', $dependenceRepository);
            $array_dependence        = explode(DIRECTORY_SEPARATOR, $dependenceRepository);
            $nameRepository          = array_pop($array_dependence);

            $dependenceValidator    = str_replace('/', DIRECTORY_SEPARATOR, $name);
            $dependenceValidator    = str_replace('Service', 'Validator', $dependenceValidator);
            $array_dependence        = explode(DIRECTORY_SEPARATOR, $dependenceValidator);
            $nameValidator          = array_pop($array_dependence);
        }
        else
        {
            $className = $name;
            $dependenceRepository = str_replace('Service', 'Repository', $name);
            $dependenceValidator = str_replace('Service', 'Validator', $name);
            $nameRepository = $dependenceRepository;
            $nameValidator = $dependenceValidator;
        }
        $stub = parent::replaceClass($stub, $className);
        return str_replace(['ClassName', 'DependenceRepository', 'NameRepository', 'DependenceValidator', 'NameValidator'],
            [$className, $dependenceRepository, $nameRepository, $dependenceValidator, $nameValidator], $stub);
    }
    /**
     * Obtém o arquivo stub para o gerador.
     *
     * @return string
     */
    protected function getStub()
    {
        return  app_path() . '/Console/Commands/Stubs/Service/make-service.stub';
    }
    /**
     * Obtém o namespace padrão para a classe.
     *
     * @param  string  $rootNamespace
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace . '\Services';
    }

    /**
     * Obtém os argumentos do comando do console.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['name', InputArgument::REQUIRED, 'The name of the service.'],
        ];
    }

}
