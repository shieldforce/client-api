<?php

namespace DummyNamespace;

use App\Repositories\DependenceRepository;
use App\Response\Response;
use App\Validations\DependenceValidator;
use Illuminate\Http\Request;

class ClassName
{
    //Variáveis Globais

    protected                            $request;
    protected                            $repository;
    protected                            $validator;
    protected                            $response;

    //Funções Padrões

    public function __construct
    (
        Request                          $request,
        NameRepository                   $repository,
        NameValidator                    $validator,
        Response                         $response,
    )
    {
        $this->request =                 $request;
        $this->repository =              $repository;
        $this->validator =               $validator;
        $this->response =                $response;
    }


    //Funções Principais

    public function index(Request $request)
    {
        try
        {
            $validator = $this->validator->index($request);
            if($validator->fails())
            {
                return $this->response->return('error','index',null, $validator->errors()->messages());
            }
            $index = $this->repository->index($request);
            return $this->response->return('success','index', null, $index);
        }
        catch (\Exception $e)
        {
            $this->configurations->createError($e);
            if(env('APP_DEBUG')=='true')
            {
                dd($e->getMessage());
            }
            return $this->response->return('error','index', null, $e->getMessage());
        }
    }

    public function create(Request $request)
    {
        try
        {
            $validator = $this->validator->create($request);
            if($validator->fails())
            {
                return $this->response->return('error','create',null, $validator->errors()->messages());
            }
            $create = $this->repository->create($request);
            return $this->response->return('success','create', null, $create);
        }
        catch (\Exception $e)
        {
            $this->configurations->createError($e);
            if(env('APP_DEBUG')=='true')
            {
                dd($e->getMessage());
            }
            return $this->response->return('error','create', null, $e->getMessage());
        }
    }

    public function store(Request $request)
    {
        try
        {
            $validator = $this->validator->store($request);
            if($validator->fails())
            {
                return $this->response->return('error','store',$request,$validator->errors()->messages());
            }
            $store = $this->repository->store($request);
            return $this->response->return('success','store', $request->all(), $store);
        }
        catch (\Exception $e)
        {
            $this->configurations->createError($e);
            if(env('APP_DEBUG')=='true')
            {
                dd($e->getMessage());
            }
            return $this->response->return('error','store', $request->all(), $e->getMessage());
        }
    }

    public function edit(Request $request)
    {
        try
        {
            $validator = $this->validator->edit($request);
            if($validator->fails())
            {
                return $this->response->return('error','edit',null, $validator->errors()->messages());
            }
            $edit = $this->repository->create($request);
            return $this->response->return('success','edit', null, $edit);
        }
        catch (\Exception $e)
        {
            $this->configurations->createError($e);
            if(env('APP_DEBUG')=='true')
            {
                dd($e->getMessage());
            }
            return $this->response->return('error','edit', null, $e->getMessage());
        }
    }

    public function update(Request $request)
    {
        try
        {
            $validator = $this->validator->update($request);
            if($validator->fails())
            {
                return $this->response->return('error','update',$request,$validator->errors()->messages());
            }
            $update = $this->repository->update($request);
            return $this->response->return('success','update', $request->all(), $update);
        }
        catch (\Exception $e)
        {
            $this->configurations->createError($e);
            if(env('APP_DEBUG')=='true')
            {
                dd($e->getMessage());
            }
            return $this->response->return('error','update', $request->all(), $e->getMessage());
        }
    }


    public function delete(Request $request)
    {
        try
        {
            $delete = $this->repository->delete($request);
            if($delete==0)
            {
                return $this->response->return('error','delete', 'Não Informado', 'Item não encontrado');
            }
            else
            {
                return $this->response->return('success','delete', 'Não Informado', "Item deletado!");
            }
        }
        catch (\Exception $e)
        {
            $this->configurations->createError($e);
            if(env('APP_DEBUG')=='true')
            {
                dd($e->getMessage());
            }
            return $this->response->return('error','delete', 'Não Informado', $e->getMessage());
        }
    }
}
