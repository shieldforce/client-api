<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class PasswordReset extends Notification
{
    use Queueable;

    protected $token;

    public function __construct($token)
    {
        $this->token = $token;
    }

    public function via($notifiable)
    {
        return ['mail'];
    }

    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Reset de Senha')
            ->from(env('MAIL_USERNAME'), env('APP_NAME'))
            ->greeting('Olá '.$notifiable['name'].'!')
            ->line('Você está recebendo este e-mail porque recebemos uma solicitação para sua conta.')
            ->action('REDEFINIR SENHA', route('password.reset', $this->token.'|||'.$notifiable['email']))
            ->line('Se você não requisitou uma redefinição de senha, nenhuma ação será necessária.')
            ->markdown('vendor.notifications.email');
    }


    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
