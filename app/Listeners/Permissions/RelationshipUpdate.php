<?php

namespace App\Listeners\Permissions;

use App\Events\Permissions\PermissionsUpdate;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class RelationshipUpdate
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PermissionsUpdate  $event
     * @return void
     */
    public function handle(PermissionsUpdate $event)
    {
        $event->permission->roles()->sync($event->roles_ids);
    }
}
