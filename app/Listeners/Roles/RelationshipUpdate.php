<?php

namespace App\Listeners\Roles;

use App\Events\Roles\RolesUpdate;
use App\Events\Users\UserUpdate;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class RelationshipUpdate
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  RolesUpdate  $event
     * @return void
     */
    public function handle(RolesUpdate $event)
    {
        $event->role->permissions()->sync($event->permissions_ids);
    }
}
