<?php
namespace App\PagSeguro;

use Illuminate\Http\Request;
use App\Models\Configuration\Configurations;
use PagSeguro\Library;
use PagSeguro\Domains\Requests\DirectPayment\Boleto;
use PagSeguro\Domains\Requests\DirectPayment\CreditCard;

class PagSeguro
{
    protected                          $configurations;
    protected                          $routeArray;
    protected                          $library;
    protected                          $boleto;
    protected                          $creditcard;

    public function __construct
    (
        Configurations                 $configurations,
        Request                        $request,
        Library                        $library,
        Boleto                         $boleto,
        CreditCard                     $creditcard
    )
    {
        $this->library =               $library;
        $this->library->initialize();
        $this->library->cmsVersion()->setName("Nome")->setRelease("1.0.0");
        $this->library->moduleVersion()->setName("Nome")->setRelease("1.0.0");
        $this->configurations =        $configurations;
        $this->routeArray =            $this->configurations->getRouteName($request);
        $this->boleto =                $boleto;
        $this->creditcard =            $creditcard;
    }

    /*
     *
     * Funções Principais
     *
     */


    //Mostrar lista ----------------------------------------------------------------------------------------------------
    public function getSession()
    {
        try
        {
            $sessionCode = \PagSeguro\Services\Session::create
            (
                \PagSeguro\Configuration\Configure::getAccountCredentials()
            );
            return $sessionCode->getResult();
        } catch (\Exception $e)
        {
            dd($e->getMessage());
        }
        return false;
    }
    //------------------------------------------------------------------------------------------------------------------

    public function clearRequest($request)
    {
        //formatando telefone
        $phone = $request['cell_phone'] ? $request['cell_phone'] : '(00)000000000';

        $ddd_1 = explode('(', $phone);
        $ddd_1a = explode(')', $ddd_1[1]);
        $ddd_2 = str_replace(['-', ' '],'',$ddd_1a);

        //formatando cpf
        $cpf_1 = $request['cpf'] ? $request['cpf'] : '00000000000';
        $cpf_2 = str_replace(['.', '-'], '', $cpf_1);

        //formatando preço
        $price_1 = $request['professional_category'] ? $request['professional_category'] : null;
        if($price_1  ==   null      ? $price_2  ='1.00'  : '');
        if($price_1  ==   'ag_20'   ? $price_2  ='20.00' : '');
        if($price_1  ==   'apl_20'  ? $price_2  ='20.00' : '');
        if($price_1  ==   'aps_50'  ? $price_2  ='50.00' : '');
        if($price_1  ==   'pp_60'   ? $price_2  ='60.00' : '');

        //formatando endereço
        $address_2 =
            [
                'street'          => 'Rua de fulano',
                'number'          => '2',
                'district'        => 'Bairro',
                'postalCode'      => '00000000',
                'city'            => 'Cidade',
                'state'           => 'RJ',
            ];


        return
            [
                'ddd'         => $ddd_2[0],
                'phone'       => $ddd_2[1],
                'cpf'         => $cpf_2,
                'price'       => $price_2,
                'address_2'   => $address_2,
            ];
    }


    // Pagar com boleto ------------------------------------------------------------------------------------------------
    public function boleto(Request $request)
    {
        $returnRequest = $this->clearRequest($request->all());
        if(env('PAGSEGURO_ENV')=='sandbox')
        {
            $email = 'emailuser@sandbox.pagseguro.com.br';
        }
        else
        {
            $email = $request['email'];
        }
        $this->boleto->setMode('DEFAULT');
        $this->boleto->setCurrency("BRL");
        $this->boleto->addItems()->withParameters(
            ''.$request['event_id'].'',
            ''.$request['event_name'].'',
            1,
            ''.$returnRequest['price'].''
        );
        $this->boleto->setReference(''.$request['event_id'].'');
        $this->boleto->setExtraAmount('0');
        $this->boleto->setSender()->setName(''.$request['first_name'].' '.$request['last_name'].'');
        $this->boleto->setSender()->setEmail(''.$email ? $email : env('MAIL_USERNAME').'');
        $this->boleto->setSender()->setPhone()->withParameters(
            ''.$returnRequest['ddd'].'',
            ''.$returnRequest['phone'].''
        );
        $this->boleto->setSender()->setDocument()->withParameters(
            'CPF',
            ''.$returnRequest['cpf'].''
        );
        $this->boleto->setSender()->setHash(''.$request['hashInput'].'');
        $this->boleto->setSender()->setIp('127.0.0.0');
        $this->boleto->setShipping()->setAddress()->withParameters(
                 ''.$returnRequest['address_2']['street'].'',
               ''.$returnRequest['address_2']['number'].'',
                ''.$returnRequest['address_2']['district'].'',
             ''.$returnRequest['address_2']['postalCode'].'',
                  ''.$returnRequest['address_2']['city'].'',
                 ''.$returnRequest['address_2']['state'].'',
               'BRA',
            ''
        );
        try {
            $result = $this->boleto->register(
                \PagSeguro\Configuration\Configure::getAccountCredentials()
            );
            //$code = $result->getCode();
            //$link = $result->getPaymentLink();

            return response()->json(['boleto'=>$result])->original;

        } catch (\Exception $e) {

            dd($e);

            $error = $e->getMessage();
        }
    }
    //------------------------------------------------------------------------------------------------------------------

    public function notification(Request $request)
    {
        $code = str_replace('-', '', $request['notificationCode']);
        $email = env('PAGSEGURO_EMAIL');
        if(env('PAGSEGURO_ENV')=='sandbox')
        {
            $token = env('PAGSEGURO_TOKEN_SANDBOX');
            $url = 'https://ws.sandbox.pagseguro.uol.com.br/v2/transactions/notifications';
        }
        else
        {
            $token = env('PAGSEGURO_TOKEN_PRODUCTION');
            $url = 'https://ws.pagseguro.uol.com.br/v2/transactions/notifications';
        }

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, "$url/$code?email=$email&token=$token");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_ENCODING, "");
        curl_setopt($curl, CURLOPT_MAXREDIRS, 10);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query(array(
            'token' => $token,
            'email' => $email,
        )));
        $response =    curl_exec($curl);
        curl_close($curl);
        $xml = simplexml_load_string($response);

        if(isset($xml) && $xml->status==3)
        {
            $inscribe = Inscribe::find($xml->reference);
            $inscribe->status =  3;
            $inscribe->save();
            $payment = PaymentsInscribes::where('inscribe_id', $inscribe->id)->where('code', $xml->code)->get()->first();
            $payment->status = 1;
            $payment->save();
        }

    }
}
