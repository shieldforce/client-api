<?php

namespace App\Providers;

use App\Events\Users\UserRegister;
use App\Events\Users\UserUpdate;
use App\Listeners\Users\RelationshipUpdate;
use App\Listeners\Users\RelationshipRegister;
use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        \App\Events\Users\UserRegister::class => [
            \App\Listeners\Users\RelationshipRegister::class,
        ],
        \App\Events\Users\UserUpdate::class => [
            \App\Listeners\Users\RelationshipUpdate::class
        ],
        \App\Events\Roles\RolesRegister::class => [
            \App\Listeners\Roles\RelationshipRegister::class,
        ],
        \App\Events\Roles\RolesUpdate::class => [
            \App\Listeners\Roles\RelationshipUpdate::class
        ],
        \App\Events\Permissions\PermissionsRegister::class => [
            \App\Listeners\Permissions\RelationshipRegister::class,
        ],
        \App\Events\Permissions\PermissionsUpdate::class => [
            \App\Listeners\Permissions\RelationshipUpdate::class
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
