<?php

    namespace App\Repositories\Home\Main;

    use App\APIIntranet\Models\ModelBase;
    use App\Models\Configuration\CmsPages;
    use App\Models\Configuration\Configurations;
    use App\Models\Configuration\Inscribes;
    use App\Models\Configuration\PaymentInscribes;
    use App\Models\Home\Main;
    use App\PagSeguro\PagSeguro;
    use Illuminate\Http\Request;


    class RepositoryMain
    {
        //Variáveis Globais

        protected                     $request;
        protected                     $model;
        protected                     $routeArray;
        protected                     $modelsIntranet;
        protected                     $pagseguro;

        //Funções Padrões

        public function __construct
        (
            Request                   $request,
            Main                      $model,
            Configurations            $configurations,
            ModelBase                 $modelsIntranet,
            PagSeguro                 $pagseguro
        )
        {
            $this->request          = $request;
            $this->model            = $model;
            $this->configurations   = $configurations;
            $this->routeArray       = $this->configurations->getRouteName($request);
            $this->modelsIntranet   = $modelsIntranet;
            $this->pagseguro        = $pagseguro;
            $this->crudName         = 'Principal do Portal';
        }

        //Main Functions

        public function index(Request $request)
        {
            $model = $this->modelsIntranet->index($request,'Events');
            $error = $this->modelsIntranet->errors($model);
            if($error==false)
                return
                    [
                        'route'       =>
                            [
                                'ambient'    => $this->routeArray[0],
                                'crud'       => $this->routeArray[1],
                                'function'   => $this->routeArray[2],
                            ],
                        'compact'     =>
                            [
                                'title'      => $this->crudName,
                                'route'      => $this->routeArray,
                                'crudName'   => $this->crudName,
                                'model'      => $model['data'],
                                'cms_page'   => CmsPages::find(1),
                            ],
                        'request'            => $request,
                    ];
            else
                return $error;
        }

        public function show(Request $request)
        {
            $request->session()->put('tokenSession', $pagSeguro = $this->pagseguro->getSession());
            $model = $this->modelsIntranet->show($request,'Events', $request['id']);
            $error = $this->modelsIntranet->errors($model);
            if($error==false)
            {
                if($model['data']['forms']==[])
                {
                    $resp = 'no-form';
                }
                elseif($model['data']['forms']!=[] && $model['data']['forms'][0]['steps']==[])
                {
                    $resp = 'no-steps';
                }
                elseif($model['data']['forms']!=[] && $model['data']['forms'][0]['steps']!=[])
                {
                    $resp = $model['data']['forms'][0]['steps'];
                }

                return
                    [
                        'route'       =>
                            [
                                'ambient'        => $this->routeArray[0],
                                'crud'           => $this->routeArray[1],
                                'function'       => $this->routeArray[2],
                            ],
                        'compact'     =>
                            [
                                'title'          => "Inscrição para " . $model['data']['name'],
                                'route'          => $this->routeArray,
                                'crudName'       => "Inscrição para " . $model['data']['name'],
                                'model'          => $model['data'],
                                'cms_page'       => CmsPages::find(1),
                                'steps'          => $resp,
                                'courses'        => $this->modelsIntranet->index($request, "Courses")['data'],
                                'request'        => $request,
                                'tokenSession'   => $request->session()->get('tokenSession'),
                            ],
                        'request'                => $request,
                    ];
            }
            else
                return $error;
        }

        public function create(Request $request)
        {
            $model = $this->model;
            return
                [
                    'route'       =>
                        [
                            'ambient'    => $this->routeArray[0],
                            'crud'       => $model['status']=='success' ? $this->routeArray[1] : 'Errors',
                            'function'   => $model['status']=='success' ? $this->routeArray[2] : 'show',
                        ],
                    'compact'     =>
                        [
                            'title'      => 'Criando '.$this->crudName,
                            'route'      => $this->routeArray,
                            'crudName'   => 'Criando '.$this->crudName,
                            'model'      => $model,
                        ],
                    'request'            => $request,
                ];
        }

        public function store(Request $request)
        {
            $model = $this->modelsIntranet->show($request,'Events', $request['event_id']);
            $storeInscribe = $this->storeInscribe($request);
            return
                [
                    'route'       =>
                        [
                            'ambient'    => $this->routeArray[0],
                            'crud'       => $this->routeArray[1],
                            'function'   => $this->routeArray[2],
                        ],
                    'compact'     =>
                        [
                            'title'      => 'Salvando '.$this->crudName,
                            'route'      => $this->routeArray,
                            'model'      => $model,
                            'boleto'     => $storeInscribe['payment'],
                            'redirect'   => env('APP_URL').'/'.'evento/'.$model['data']['id'].'/'.$model['data']['tag']
                        ],
                    'request'            => $request,
                ];
        }

        public function edit(Request $request)
        {
            $model = $this->model;
            return
                [
                    'route'       =>
                        [
                            'ambient'    => $this->routeArray[0],
                            'crud'       => $model['status']=='success' ? $this->routeArray[1] : 'Errors',
                            'function'   => $model['status']=='success' ? $this->routeArray[2] : 'show',
                        ],
                    'compact'     =>
                        [
                            'title'      => 'Editando '.$this->crudName,
                            'route'      => $this->routeArray,
                            'model'      => $model,
                            'item'       => $model,
                            'crudName'   => 'Editando Item de ID :',
                        ],
                    'request'            => $request,
                ];
        }

        public function update(Request $request)
        {
            $model = $this->model;
            return
                [
                    'route'       =>
                        [
                            'ambient'    => $this->routeArray[0],
                            'crud'       => $model['status']=='success' ? $this->routeArray[1] : 'Errors',
                            'function'   => $model['status']=='success' ? $this->routeArray[2] : 'show',
                        ],
                    'compact'     =>
                        [
                            'title'      => 'Atualiando '.$this->crudName,
                            'route'      => $this->routeArray,
                            'model'      => $model,
                        ],
                    'request'            => $request,
                ];
        }

        public function delete(Request $request)
        {
            $model = $this->model;
            return
                [
                    'route'       =>
                        [
                            'ambient'    => $this->routeArray[0],
                            'crud'       => $model['status']=='success' ? $this->routeArray[1] : 'Errors',
                            'function'   => $model['status']=='success' ? $this->routeArray[2] : 'show',
                        ],
                    'compact'     =>
                        [
                            'title'      => 'Deletando '.$this->crudName,
                            'route'      => $this->routeArray,
                            'model'      => $model,
                        ],
                    'request'            => $request,
                ];
        }

        public function restore(Request $request)
        {
            $model = $this->model;
            return
                [
                    'route'       =>
                        [
                            'ambient'    => $this->routeArray[0],
                            'crud'       => $model['status']=='success' ? $this->routeArray[1] : 'Errors',
                            'function'   => $model['status']=='success' ? $this->routeArray[2] : 'show',
                        ],
                    'compact'     =>
                        [
                            'title'      => $this->crudName,
                            'route'      => $this->routeArray,
                            'model'      => $model,
                        ],
                    'request'            => $request,
                ];
        }

        public function validationEmailInscribes(Request $request)
        {
            $person = $this->modelsIntranet->show($request, "Person", $request['email']);
            $inscribe = Inscribes::where('person_id', $person['data']['id'])
                ->where('event_id', $request->event_id)
                ->with('payments')
                ->get()
                ->first();
            return ['person'=>$person, 'inscribe'=> $inscribe];
        }
        public function validationCPFInscribes(Request $request)
        {
            $request['cpf'] = str_replace(['-', '.'],'',$request['cpf']);
            $person = $this->modelsIntranet->show($request, "Person", $request['cpf']);
            $inscribe = Inscribes::where('person_id', $person['data']['id'])
                ->where('event_id', $request->event_id)
                ->with('payments')
                ->get()
                ->first();
            return ['person'=>$person, 'inscribe'=> $inscribe];
        }
        public function validationNumberRegistrationInscribes(Request $request)
        {
            $request['number_registration'] = str_replace('-','',$request['number_registration']);
            $person = $this->modelsIntranet->show($request, "Person", $request['number_registration']);
            $inscribe = Inscribes::where('person_id', $person['data']['id'])
                ->where('event_id', $request->event_id)
                ->with('payments')
                ->get()
                ->first();
            return ['person'=>$person, 'inscribe'=> $inscribe];
        }

        public function validationNumberRegistrationInscribesSearch(Request $request)
        {
            $request['number_registration'] = str_replace('-','',$request['number_registration']);
            $person = $this->modelsIntranet->show($request, "Person", $request['number_registration']);

            $inscribe = Inscribes::where('registration_id', $person['data']['registration']['id'] ?? $person['data']['registration']['id'])
                ->where('event_id', $request->event_id)
                ->with('payments')
                ->get()
                ->first();
            return ['person'=>$person, 'inscribe'=> $inscribe];
        }

        public function search(Request $request)
        {
            $model = $this->modelsIntranet->show($request,'Events', $request['id']);
            return
                [
                    'route'       =>
                        [
                            'ambient'        => $this->routeArray[0],
                            'crud'           => $this->routeArray[1],
                            'function'       => $this->routeArray[2],
                        ],
                    'compact'     =>
                        [
                            'title'          => "Consulta de Inscrição",
                            'route'          => $this->routeArray,
                            'crudName'       => "Consulta de Inscrição",
                            'model'          => $model['data'],
                            'cms_page'       => CmsPages::find(1),
                            'request'        => $request,
                        ],
                    'request'                => $request,
                ];

        }

        public function storeInscribe($request)
        {
            $returnClear = $this->clearRequest($request);
            $request['cpf'] = $returnClear['cpf'];
            $request['birth'] = $returnClear['birth'];
            $return = $this->modelsIntranet->store($request,'Person');

            if($return['data']['registration']!=[])
            {
                $valInscribeExist = Inscribes::where('registration_id', $return['data']['registration']['id'])->get()->first();
                if($valInscribeExist==null)
                {
                    $inscribe = Inscribes::create(['registration_id'=>$return['data']['registration']['id'], 'event_id'=>$request['event_id'], 'status'=>1]);
                }
                else
                {
                    $inscribe = $valInscribeExist;
                }
            }
            if($return['data']['person']!=[])
            {
                $valInscribeExist = Inscribes::where('person_id', $return['data']['person']['id'])->get()->first();
                if($valInscribeExist==null)
                {
                    $inscribe = Inscribes::create(['person_id'=>$return['data']['person']['id'], 'event_id'=>$request['event_id'], 'status'=>1]);
                }
                else
                {
                    $inscribe = $valInscribeExist;
                }
            }

            $valPayment =  PaymentInscribes::where('inscribe_id', $inscribe['id'])->where('expiry_date', '>', date('Y-m-d'))->get()->first();
            if($valPayment==null)
            {
                $boleto = $this->pagseguro->boleto($request);
                $code = response()->json($boleto)->original['boleto']->getCode();
                $link = response()->json($boleto)->original['boleto']->getPaymentLink();
                //-----------------------------------------------------------------------
                $payment = PaymentInscribes::create([
                    'inscribe_id'      => $inscribe->id,
                    'type'             => 'boleto',
                    'name'             => $request['event_name'],
                    'value'            => $returnClear['price'],
                    'inscribe_date'    => $inscribe->created_at,
                    'expiry_date'      => now()->addDays(3),
                    'link_boleto'      => $link,
                    'code'             => $code,
                    'status'           => 1,
                    'email_confirmed'  => 0
                ]);
            }
            else
            {
                $payment = $valPayment;
            }
            return ['inscribe'=>$inscribe, 'payment'=>$payment];
        }


        public function clearRequest($request)
        {
            //formatando telefone
            $phone = $request['cell_phone'] ? $request['cell_phone'] : '(00)000000000';

            $ddd_1 = explode('(', $phone);
            $ddd_1a = explode(')', $ddd_1[1]);
            $ddd_2 = str_replace(['-', ' '],'',$ddd_1a);

            //formatando cpf
            $cpf_1 = $request['cpf'] ? $request['cpf'] : '00000000000';
            $cpf_2 = str_replace(['.', '-'], '', $cpf_1);

            //formatando preço
            $price_1 = $request['professional_category'] ? $request['professional_category'] : null;
            if($price_1  ==   null      ? $price_2  ='1.00'  : '');
            if($price_1  ==   'ag_20'   ? $price_2  ='20.00' : '');
            if($price_1  ==   'apl_20'  ? $price_2  ='20.00' : '');
            if($price_1  ==   'aps_50'  ? $price_2  ='50.00' : '');
            if($price_1  ==   'pp_60'   ? $price_2  ='60.00' : '');

            //formatando endereço
            $address_2 =
                [
                    'street'          => 'Rua de fulano',
                    'number'          => '2',
                    'district'        => 'Bairro',
                    'postalCode'      => '00000000',
                    'city'            => 'Cidade',
                    'state'           => 'RJ',
                ];

            //formatando aniversário
            $birth_1 = explode('/',$request['birth']);
            $birth_2 = $birth_1[2].'-'.$birth_1[1].'-'.$birth_1[0];


            return
                [
                    'ddd'         => $ddd_2[0],
                    'phone'       => $ddd_2[1],
                    'cpf'         => $cpf_2,
                    'price'       => $price_2,
                    'address_2'   => $address_2,
                    'birth'       => $birth_2
                ];
        }

    }




