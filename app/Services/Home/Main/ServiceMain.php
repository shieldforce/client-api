<?php

namespace App\Services\Home\Main;

use App\Models\Configuration\Configurations;
use App\Models\Home\Main;
use App\Repositories\Home\Main\RepositoryMain;
use App\Response\Response;
use App\Validations\Home\Main\ValidatorMain;
use Illuminate\Http\Request;

class ServiceMain
{
    //Variáveis Globais

    protected                            $request;
    protected                            $repository;
    protected                            $validator;
    protected                            $response;
    protected                            $model;
    protected                            $configurations;

    //Funções Padrões

    public function __construct
    (
        Request                          $request,
        RepositoryMain                   $repository,
        ValidatorMain                    $validator,
        Response                         $response,
        Main                             $model,
        Configurations                   $configurations
    )
    {
        $this->request =                 $request;
        $this->repository =              $repository;
        $this->validator =               $validator;
        $this->response =                $response;
        $this->model =                   $model;
        $this->configurations =          $configurations;
    }


    //Funções Principais

    public function index(Request $request)
    {
        try
        {
            $validator = $this->validator->index($request);
            if($validator->fails())
            {
                return $this->response->return('error','index',null, $validator->errors()->messages());
            }
            $index = $this->repository->index($request);
            return $this->response->return('success','index', null, $index);
        }
        catch (\Exception $e)
        {
            $this->configurations->createError($e);
            if(env('APP_DEBUG')=='true')
            {
                dd($e->getMessage());
            }
            return $this->response->return('error','index', null, $e->getMessage());
        }
    }

    public function show(Request $request)
    {
        try
        {
            $validator = $this->validator->show($request);
            if($validator->fails())
            {
                return $this->response->return('error','show',null, $validator->errors()->messages());
            }
            $show = $this->repository->show($request);
            return $this->response->return('success','show', null, $show);
        }
        catch (\Exception $e)
        {
            $this->configurations->createError($e);
            if(env('APP_DEBUG')=='true')
            {
                dd($e->getMessage());
            }
            return $this->response->return('error','show', null, $e->getMessage());
        }
    }

    public function create(Request $request)
    {
        try
        {
            $validator = $this->validator->create($request);
            if($validator->fails())
            {
                return $this->response->return('error','create',null, $validator->errors()->messages());
            }
            $create = $this->repository->create($request);
            return $this->response->return('success','create', null, $create);
        }
        catch (\Exception $e)
        {
            $this->configurations->createError($e);
            if(env('APP_DEBUG')=='true')
            {
                dd($e->getMessage());
            }
            return $this->response->return('error','create', null, $e->getMessage());
        }
    }

    public function store(Request $request)
    {
        try
        {
            $validator = $this->validator->store($request);
            if($validator->fails())
            {
                return $this->response->return('error','store',$request,$validator->errors()->messages());
            }
            $store = $this->repository->store($request);
            return $this->response->return('success','store', $request->all(), $store);
        }
        catch (\Exception $e)
        {
            $this->configurations->createError($e);
            if(env('APP_DEBUG')=='true')
            {
                dd($e->getMessage());
            }
            return $this->response->return('error','store', $request->all(), $e->getMessage());
        }
    }

    public function edit(Request $request)
    {
        try
        {
            $validator = $this->validator->edit($request);
            if($validator->fails())
            {
                return $this->response->return('error','edit',null, $validator->errors()->messages());
            }
            $edit = $this->repository->create($request);
            return $this->response->return('success','edit', null, $edit);
        }
        catch (\Exception $e)
        {
            $this->configurations->createError($e);
            if(env('APP_DEBUG')=='true')
            {
                dd($e->getMessage());
            }
            return $this->response->return('error','edit', null, $e->getMessage());
        }
    }

    public function update(Request $request)
    {
        try
        {
            $validator = $this->validator->update($request);
            if($validator->fails())
            {
                return $this->response->return('error','update',$request,$validator->errors()->messages());
            }
            $update = $this->repository->update($request);
            return $this->response->return('success','update', $request->all(), $update);
        }
        catch (\Exception $e)
        {
            $this->configurations->createError($e);
            if(env('APP_DEBUG')=='true')
            {
                dd($e->getMessage());
            }
            return $this->response->return('error','update', $request->all(), $e->getMessage());
        }
    }


    public function delete(Request $request)
    {
        try
        {
            $delete = $this->repository->delete($request);
            if($delete==0)
            {
                return $this->response->return('error','delete', $request, 'Item não encontrado');
            }
            else
            {
                return $this->response->return('success','delete', $request, "Item deletado!");
            }
        }
        catch (\Exception $e)
        {
            $this->configurations->createError($e);
            if(env('APP_DEBUG')=='true')
            {
                dd($e->getMessage());
            }
            return $this->response->return('error','delete', $request, $e->getMessage());
        }
    }

    public function restore(Request $request)
    {
        try
        {
            $validator = $this->validator->restore($request);
            if($validator->fails())
            {
                return $this->response->return('error','restore',$request,$validator->errors()->messages());
            }
            $restore = $this->repository->restore($request);
            return $this->response->return('success','restore', $request->all(), $restore);
        }
        catch (\Exception $e)
        {
            $this->configurations->createError($e);
            if(env('APP_DEBUG')=='true')
            {
                dd($e->getMessage());
            }
            return $this->response->return('error','restore', $request, $e->getMessage());
        }
    }

    public function validationEmailInscribes(Request $request)
    {
        return $validationEmailInscribes = $this->repository->validationEmailInscribes($request);
    }

    public function validationCPFInscribes(Request $request)
    {
        return $validationCPFInscribes = $this->repository->validationCPFInscribes($request);
    }

    public function validationNumberRegistrationInscribes(Request $request)
    {
        return $validationCPFInscribes = $this->repository->validationNumberRegistrationInscribes($request);
    }

    public function search(Request $request)
    {
        try
        {
            $validator = $this->validator->search($request);
            if($validator->fails())
            {
                return $this->response->return('error','search',null, $validator->errors()->messages());
            }
            $search = $this->repository->search($request);
            return $this->response->return('success','search', null, $search);
        }
        catch (\Exception $e)
        {
            $this->configurations->createError($e);
            if(env('APP_DEBUG')=='true')
            {
                dd($e->getMessage());
            }
            return $this->response->return('error','search', null, $e->getMessage());
        }
    }

}
