<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Configuration\Configurations;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/Panel/Main';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected                          $configurations;

    public function __construct
    (
        Configurations $configurations,
        Request $request
    )
    {
        $this->middleware('guest')->except('logout');
        $this->configurations = $configurations;
    }

    public function showLoginForm(Request $request)
    {
        $title = 'Faça login';
        $route = ['auth','main','login'];
        $ipaddress = $this->configurations->ipClient();
        return view('auth.login', compact('ipaddress', 'request', 'route', 'title'));
    }
}
