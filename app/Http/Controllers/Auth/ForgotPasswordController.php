<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use App\Models\Configuration\Configurations;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    protected                     $configurations;
    protected                     $routeArray;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Configurations $configurations, Request $request)
    {
        $this->middleware('guest');
        $this->request =          $request;
        $this->configurations =   $configurations;
        $this->routeArray =       $this->configurations->getRouteName($request);
    }

    public function showLinkRequestForm(Request $request)
    {
        $title = 'Enviando Link de Reset de Senha';
        $route = ['auth','main','linkRequest'];
        $ipaddress = $this->configurations->ipClient();
        return view('auth.passwords.email', compact('ipaddress', 'request', 'route', 'title'));
    }
}
