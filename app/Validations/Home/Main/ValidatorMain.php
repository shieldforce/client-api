<?php

namespace App\Validations\Home\Main;

use App\Models\Home\Main;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class ValidatorMain
{
    //Variáveis Globais

    protected                     $request;
    protected                     $model;

    //Funções Padrões

    public function __construct
    (
        Request                   $request,
        Main                      $model
    )
    {
        $this->request =          $request;
        $this->model =            $model;
    }


    //Funções Principais

    public function index(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [

            ]
        );
        return $validator;
    }

    public function show(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [

            ]
        );
        return $validator;
    }

    public function create(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [

            ]
        );
        return $validator;
    }

    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [

            ]
        );
        return $validator;
    }

    public function edit(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [

            ]
        );
        return $validator;
    }

    public function update(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [

            ]
        );
        return $validator;
    }

    public function delete(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [

            ]
        );
        return $validator;
    }

    public function restore(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [

            ]
        );
        return $validator;
    }

    public function search(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [

            ]
        );
        return $validator;
    }
}
