<?php

namespace App\Validations\Panel\CmsPages;

use App\Models\Configuration\CmsPages;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class ValidatorCmsPages
{
    //Variáveis Globais

    protected                     $request;
    protected                     $model;

    //Funções Padrões

    public function __construct
    (
        Request                   $request,
        CmsPages                  $model
    )
    {
        $this->request =          $request;
        $this->model =            $model;
    }


    //Funções Principais

    public function index(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [

            ]
        );
        return $validator;
    }

    public function show(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [

            ]
        );
        return $validator;
    }

    public function create(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [

            ]
        );
        return $validator;
    }

    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'page_name'                 => ['required'],
                'logo1'                     => ['required'],
                'facebook'                  => ['required'],
                'twitter'                   => ['required'],
                'session_1_img_background'  => ['required'],
                'session_1_title'           => ['required'],
                'session_1_subtitle'        => ['required'],
                'session_2_img_right'       => ['required'],
                'session_2_title'           => ['required'],
                'session_2_content'         => ['required'],
                'session_3_title'           => ['required'],
                'session_3_subtitle'        => ['required'],

            ]
        );
        return $validator;
    }

    public function edit(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [

            ]
        );
        return $validator;
    }

    public function update(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'page_name'                 => ['required'],
                'facebook'                  => ['required'],
                'twitter'                   => ['required'],
                'session_1_title'           => ['required'],
                'session_1_subtitle'        => ['required'],
                'session_2_title'           => ['required'],
                'session_2_content'         => ['required'],
                'session_3_title'           => ['required'],
                'session_3_subtitle'        => ['required'],
            ]
        );
        return $validator;
    }

    public function delete(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [

            ]
        );
        return $validator;
    }

    public function restore(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [

            ]
        );
        return $validator;
    }
}
