<?php

namespace App\Validations\Panel\Errors;

use App\Models\Configuration\Errors;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class ValidatorErrors
{
    //Variáveis Globais

    protected                     $request;
    protected                     $model;

    //Funções Padrões

    public function __construct
    (
        Request                   $request,
        Errors                    $model
    )
    {
        $this->request =          $request;
        $this->model =            $model;
    }


    //Funções Principais

    public function fixed(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [

            ]
        );
        return $validator;
    }

    public function index(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [

            ]
        );
        return $validator;
    }

    public function show(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [

            ]
        );
        return $validator;
    }

    public function create(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [

            ]
        );
        return $validator;
    }

    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'user_id'                => ['required'],
                'title'                  => ['required'],
                'code'                   => ['required'],
                'content'                => ['required'],
                'status'                 => ['required'],
            ]
        );
        return $validator;
    }

    public function edit(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [

            ]
        );
        return $validator;
    }

    public function update(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'status'                 => ['required'],
            ]
        );
        return $validator;
    }

    public function delete(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [

            ]
        );
        return $validator;
    }

    public function restore(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [

            ]
        );
        return $validator;
    }
}
