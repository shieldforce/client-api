<?php

namespace App\Validations\Panel\Permissions;

use App\Models\Configuration\Permissions;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class ValidatorPermissions
{
    //Variáveis Globais

    protected                     $request;
    protected                     $model;

    //Funções Padrões

    public function __construct
    (
        Request                   $request,
        Permissions               $model
    )
    {
        $this->request =          $request;
        $this->model =            $model;
    }


    //Funções Principais

    public function index(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [

            ]
        );
        return $validator;
    }

    public function show(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [

            ]
        );
        return $validator;
    }

    public function create(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [

            ]
        );
        return $validator;
    }

    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'name'                       => ['required'],
                'label'                      => ['required'],
                'group'                      => ['required'],
            ]
        );
        return $validator;
    }

    public function edit(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [

            ]
        );
        return $validator;
    }

    public function update(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'label'                      => ['required'],
            ]
        );
        return $validator;
    }

    public function delete(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [

            ]
        );
        return $validator;
    }

    public function restore(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [

            ]
        );
        return $validator;
    }
}
