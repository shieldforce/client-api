@extends('auth.Template.index')

@section('content')

    <div class="login-box">
        <div class="login-logo">
            <img src="{{ asset('HomeP/img/logo.png') }}"/>
        </div>
        <div class="card">
            <div class="card-body login-card-body">
                <p class="login-box-msg">Acessar {{ env('APP_NAME') }}</p>

                <form action="{{ route('login') }}" method="post">

                    {{ csrf_field() }}

                    @if ($errors->has('email'))
                        <span style="color: red;">{{ $errors->first('email') }}</span>
                    @endif
                    <div class="input-group mb-3">
                        <input type="email" name="email" class="form-control {{ $errors->has('email') ? 'error-login' : '' }}" placeholder="Email">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fa fa-envelope"></span>
                            </div>
                        </div>
                    </div>

                    @if ($errors->has('password'))
                        <span style="color: red;">{{ $errors->first('password') }}</span>
                    @endif
                    <div class="input-group mb-3">
                        <input type="password" name="password" class="form-control {{ $errors->has('password') ? 'error-login' : '' }}" minlength="8" placeholder="Senha">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-8">
                            <div class="icheck-primary">
                                <input type="checkbox" id="remember">
                                <label for="remember">
                                    Lembrar-me
                                </label>
                            </div>
                        </div>

                        <div class="col-4">
                            <button type="submit" class="btn btn-primary btn-block btn-flat">Acessar</button>
                        </div>

                    </div>
                </form>

                <hr>

                <p class="mb-1">
                    <a href="{{ route('password.request') }}">Recuperar a Senha</a>
                </p>
                <p class="text-muted m-t-25 m-b-0 p-0">Ainda não possui conta?<a href="{{ route('register') }}"> Cadastrar</a></p>

            </div>

        </div>
    </div>

@endsection
