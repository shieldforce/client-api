@extends('auth.Template.index')

@section('content')

    <div class="login-box">
        <div class="login-logo">
            <img src="{{ asset('HomeP/img/logo.png') }}"/>
        </div>
        <div class="card">
            <div class="card-body login-card-body">
                <h5 class="sign-in-heading text-center">Redefinição de Senha!</h5>
                <p class="text-center text-muted">Insira seu e-mail e a nova senha</p>

                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif

                <form action="{{ route('password.update') }}" method="post">

                    {{ csrf_field() }}
                    <input type="hidden" name="token" value="{{ $token }}">

                    @if ($errors->has('email'))
                        <span style="color: red;">{{ $errors->first('email') }}</span>
                    @endif
                    <div class="input-group mb-3">
                        <input type="email" name="email" class="form-control {{ $errors->has('email') ? 'error-login' : '' }}" readonly value="{{ $email }}">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fa fa-envelope"></span>
                            </div>
                        </div>
                    </div>

                    @if ($errors->has('password'))
                        <span style="color: red;">{{ $errors->first('password') }}</span>
                    @endif
                    <div class="input-group mb-3">
                        <input type="password" name="password" class="form-control {{ $errors->has('password') ? 'error-login' : '' }}" minlength="8" placeholder="Digite a Nova Senha">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div>
                        </div>
                    </div>

                    <div class="input-group mb-3">
                        <input type="password" name="password_confirmation" class="form-control" minlength="8" placeholder="Repita a Nova Senha">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div>
                        </div>
                    </div>

                    <div class="row">

                        <div class="col-12">
                            <button type="submit" class="btn btn-primary btn-block btn-flat">Resetar Senha</button>
                        </div>

                    </div>
                </form>

                <hr>

                <p class="text-muted m-t-25 m-b-0 p-0">Lembrei minha Senha : <a href="{{ route('login') }}"> Acessar</a></p>

            </div>

        </div>
    </div>

@endsection
