@extends('auth.Template.index')

@section('content')

    <div class="login-box">
        <div class="login-logo">
            <img src="{{ asset('HomeP/img/logo.png') }}"/>
        </div>
        <div class="card">
            <div class="card-body login-card-body">
                <p class="login-box-msg">E-mail será enviado com o Link!</p>

                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif

                <form action="{{ route('password.email') }}" method="post">

                    {{ csrf_field() }}

                    @if ($errors->has('email'))
                        <span style="color: red;">{{ $errors->first('email') }}</span>
                    @endif
                    <div class="input-group mb-3">
                        <input type="email" name="email" class="form-control {{ $errors->has('email') ? 'error-login' : '' }}" placeholder="Email">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fa fa-envelope"></span>
                            </div>
                        </div>
                    </div>

                    <div class="row">

                        <div class="col-12">
                            <button type="submit" class="btn btn-primary btn-block btn-flat">Enviar Senha</button>
                        </div>

                    </div>
                </form>

                <hr>

                <p class="text-muted m-t-25 m-b-0 p-0">Ainda não possui conta?<a href="{{ route('register') }}"> Cadastrar</a></p>
                <p class="text-muted m-t-25 m-b-0 p-0">Já possui conta?<a href="{{ route('login') }}"> Acessar</a></p>

            </div>

        </div>
    </div>

@endsection
