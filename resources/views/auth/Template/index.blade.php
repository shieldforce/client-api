<!DOCTYPE html>
<html lang="pt-br">
    @includeIf("$route[0].Template.head")
    <body class="hold-transition login-page" style="height: auto !important;background-image: url(../../AuthP/img/fundo_login.jpg);">
        <div class="container">
            @yield('content')
        </div>
        @includeIf("$route[0].Template.javascript")
    </body>
</html>






