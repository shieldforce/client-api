<footer class="clearfix">
    <div class="container">
        <p>© {{ date('Y') }} UNIG Digital</p>
        <ul>
            <li><a href="#0" class="animated_link">Portal</a></li>
            <li><a href="#0" class="animated_link">Notícias</a></li>
            <li><a href="#0" class="animated_link">Contato</a></li>
        </ul>
    </div>
</footer>
<!-- end footer-->
