<!---------------------------------------------------------->
<!--------------- DIVS PARA AVISO DE SISTEMA --------------->
<!---------------------------------------------------------->
<script src="{{ asset('PanelP/plugins/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('PanelP/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
<script>
    $.widget.bridge('uibutton', $.ui.button)
</script>
<script src="{{ asset('PanelP/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('PanelP/plugins/chart.js/Chart.min.js') }}"></script>
<script src="{{ asset('PanelP/plugins/sparklines/sparkline.js') }}"></script>
<script src="{{ asset('PanelP/plugins/jqvmap/jquery.vmap.min.js') }}"></script>
<script src="{{ asset('PanelP/plugins/jqvmap/maps/jquery.vmap.world.js') }}"></script>
<script src="{{ asset('PanelP/plugins/jquery-knob/jquery.knob.min.js') }}"></script>
<script src="{{ asset('PanelP/plugins/moment/moment.min.js') }}"></script>
<script src="{{ asset('PanelP/plugins/daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('PanelP/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>
<script src="{{ asset('PanelP/plugins/summernote/summernote-bs4.min.js') }}"></script>
<script src="{{ asset('PanelP/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
<script src="{{ asset('PanelP/plugins/fastclick/fastclick.js') }}"></script>
<script src="{{ asset('PanelP/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
<script src="{{ asset('PanelP/plugins/toastr/toastr.min.js') }}"></script>
<script src="{{ asset('PanelP/dist/js/adminlte.js') }}"></script>
<script src="{{ asset('PanelP/dist/js/pages/dashboard.js') }}"></script>
<script src="{{ asset('PanelP/dist/js/demo.js') }}"></script>
<script type="text/javascript">
    $(function() {
        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000
        });

        $(document).ready(function () {
            @if(session('error'))
            toastr.error("{{ session('error') }}");
            @endif
            @if(session('success'))
            toastr.success("{{ session('success') }}");
            @endif
            @if(session('info'))
            toastr.info("{{ session('info') }}");
            @endif
            @if(session('warning'))
            toastr.warning("{{ session('warning') }}");
            @endif
            @if($errors->has('email') || $errors->has('password'))
            toastr.warning("{{ $errors->first('email') }} | {{ $errors->first('password') }}");
            @endif
        });
    });

</script>
<!---------------------------------------------------------->
<!---------------------------------------------------------->
<!---------------------------------------------------------->
