<header>
    <div class="container-fluid">
        <div class="row">
            <div class="col-3">
                <a href="{{ route("Home.Main.index") }}"><img src="{{ $cms_page->logo1!=null && $cms_page->logo1!='default' ? asset("HomeP/img/CmsPages/$cms_page->logo1") : asset("HomeP/img/CmsPages/logo1.png") }}" alt="" width="35" height="35"></a>
            </div>
            <div class="col-9">
                <div id="social">
                    <ul>
                        @if($cms_page->facebook!=null)
                            <li><a href="{{ $cms_page->facebook }}"><i class="icon-facebook"></i></a></li>
                        @endif
                        @if($cms_page->twitter!=null)
                            <li><a href="{{ $cms_page->twitter }}"><i class="icon-twitter"></i></a></li>
                        @endif
                        @if($cms_page->google!=null)
                            <li><a href="{{ $cms_page->google }}"><i class="icon-google"></i></a></li>
                        @endif
                        @if($cms_page->linkedin!=null)
                            <li><a href="{{ $cms_page->linkedin }}"><i class="icon-linkedin"></i></a></li>
                        @endif

                        @if($cms_page->youtube!=null)
                            <li><a href="{{ $cms_page->youtube }}"><i class="icon-youtube"></i></a></li>
                        @endif
                        @if($cms_page->instagram!=null)
                            <li><a href="{{ $cms_page->instagram }}"><i class="icon-instagram"></i></a></li>
                        @endif
                    </ul>
                </div>
                <!-- /social -->
                <a href="#0" class="cd-nav-trigger">Menu<span class="cd-icon"></span></a>
                <!-- /menu button -->
                <nav>
                    <ul class="cd-primary-nav">
                        <li><a href="{{ route("Home.Main.index") }}" class="animated_link">Home</a></li>
                    </ul>
                </nav>
                <!-- /menu -->
            </div>
        </div>
    </div>
    <!-- /container -->
</header>
<!-- /Header -->
