<script>
    //Variáreis ------------------------------------------------------------------------------------
    var policy                                 = $('#policy');
    var policy_fadeout                         = $('.policy_fadeout');
    var terms                                  = $('#terms');
    var terms_fadeou                           = $('.terms_fadeou');
    var no_student                             = $('#no_student');
    var number_registration                    = $('.number_registration');
    var error_number_registration_null         = $('.error_number_registration_null');
    var forward_search                         = $('#forward_search');
    var forward                                = $('#forward');
    var reset                                  = $('#reset');
    var loading_person                         = $('#loading_person');
    var person_success                         = $('#person_success');
    var person_error                           = $('#person_error');
    var loading_inscribe                       = $('#loading_inscribe');
    var inscribe_success                       = $('#inscribe_success');
    var inscribe_error                         = $('#inscribe_error');
    var loading_email                          = $('#loading_email');
    var person_email_success                   = $('#person_email_success');
    var person_email_error                     = $('#person_email_error');
    var loading_email_inscribe                 = $('#loading_email_inscribe');
    var inscribe_email_success                 = $('#inscribe_email_success');
    var inscribe_email_error                   = $('#inscribe_email_error');
    var loading_cpf                            = $('#loading_cpf');
    var person_cpf_success                     = $('#person_cpf_success');
    var person_cpf_error                       = $('#person_cpf_error');
    var loading_cpf_inscribe                   = $('#loading_cpf_inscribe');
    var inscribe_cpf_success                   = $('#inscribe_cpf_success');
    var inscribe_cpf_error                     = $('#inscribe_cpf_error');
    var cpf_invalidated                        = $('#cpf_invalidated');
    var button_submit                          = $('.submit_finish');
    var button_backward                        = $('#backward');
    var email_data_use                         = $('#email_data_use');
    var email_data_clear                       = $('#email_data_clear');
    var cpf_data_use                           = $('#cpf_data_use');
    var cpf_data_clear                         = $('#cpf_data_clear');
    var search_number_registration             = $('#search_number_registration');
    var search_cpf                             = $('#search_cpf');
    var search_2                               = $('#search_2');
    var infor_person                           = $('.infor_person');
    var infor_inscribe                         = $('.infor_inscribe');
    var infor_payment                          = $('.infor_payment');
    //-------------------------------------
    var registration_id                        = $('#registration_id');
    var person_id                              = $('#person_id');
    var first_name                             = $('#first_name');
    var last_name                              = $('#last_name');
    var email                                  = $('#email');
    var cpf                                    = $('#cpf');
    var cpf_hidden                             = $('#cpf_hidden');
    var birth                                  = $('#birth');
    var birth_hidden                           = $('#birth_hidden');
    var cell_phone                             = $('#cell_phone');
    var cell_phone_hidden                      = $('#cell_phone_hidden');
    var email_hidden                           = $('#email_hidden');
    var registration_type                      = $('#registration_type');
    var professional_category                  = $('#professional_category');
    var loading_payment                        = $('.loading_payment');
    //Start pagseguro--------------------------------------------------------------------------------
    var hashInput                              = $('#hashInput');
    PagSeguroDirectPayment.setSessionId("{{ isset($tokenSession) ? $tokenSession : '' }}");
    var hash = PagSeguroDirectPayment.getSenderHash();
    hashInput.val(hash);

    PagSeguroDirectPayment.onSenderHashReady(function(response){
        if(response.status == 'error') {
            console.log(response.message);
            return false;
        }
        $('#hashInput').val(response.senderHash);
    });

    //-----------------------------------------------------------------------------------------------
    //esconder botões
    function clearAlerts()
    {
        error_number_registration_null.hide();
        reset.hide();
        forward.hide();
        loading_person.hide();
        person_error.hide();
        loading_inscribe.hide();
        inscribe_success.hide();
        inscribe_error.hide();
        loading_email.hide();
        person_success.hide();
        person_email_success.hide();
        loading_email_inscribe.hide();
        inscribe_email_success.hide();
        inscribe_email_error.hide();
        loading_cpf.hide();
        person_cpf_success.hide();
        loading_cpf_inscribe.hide();
        inscribe_cpf_success.hide();
        inscribe_cpf_error.hide();
        cpf_invalidated.hide();
        cpf_hidden.hide();
        birth_hidden.hide();
        cell_phone_hidden.hide();
        email_hidden.hide();
        email_data_use.hide();
        email_data_clear.hide();
        cpf_data_use.hide();
        cpf_data_clear.hide();
        loading_payment.hide();
    }
    clearAlerts();
    //-----------------------------------------------------------------------------------------------
    function removeSessionAll()
    {
        //Limpar os itens no cache de session se existir
        if(window.sessionStorage.getItem('person')){ window.sessionStorage.removeItem('person')}
        if(window.sessionStorage.getItem('inscribe')){ window.sessionStorage.removeItem('inscribe')}
    }
    //-----------------------------------------------------------------------------------------------
    function getSessionPerson()
    {
        if(window.sessionStorage.getItem('person')){var getPerson = window.sessionStorage.getItem('person')}
        return JSON.parse(getPerson)['data'];
    }
    function getSessionInscribe()
    {
        if(window.sessionStorage.getItem('inscribe')){var getInscribe = window.sessionStorage.getItem('inscribe')}
        return JSON.parse(getInscribe);
    }
    //-----------------------------------------------------------------------------------------------
    policy.click(function () {
        //Checando checkbox de políticas ao aceitar o modal
        policy_fadeout.prop('checked', true);
    });
    terms.click(function () {
        //Checando checkbox de termos ao aceitar o modal
        terms_fadeou.prop('checked', true);
    });
    no_student.click(function () {
        clearAlerts();
        removeSessionAll();
        if(no_student.is(":checked")===true)
        {
            //Se checkbox de não sou aluno estiver checado faça isso
            number_registration.prop('required', false).removeClass('required');
            error_number_registration_null.fadeOut();
            removeSessionAll();
            clearDataInputsPerson();
            forward_search.hide();
            forward.fadeIn();
        }
        else
        {
            //Se checkbox de "não sou aluno" não estiver checado faça isso
            number_registration.prop('required', true).addClass('required');
            forward_search.fadeIn();
            forward.hide();
        }
    });
</script>

<script>

    //Verificação por matrícula

    var number_registration_exist = document.getElementById("number_registration");
    //Se campo number_registration existir faça isso
    if(number_registration_exist!==null)
    {
        //Se clicarr no input number_registration resetar a verificação
        number_registration.focusin(function () {
            forward_search.show();
            forward.hide();
        });

        //Se clicar no botão forward_search faça isso
        forward_search.click(function () {
            validateNumberRegistration();
        });
    }
    //Se campo number_registration não existir faça isso
    else
    {

    }

    function validateNumberRegistration() {
        //Limpar os itens no cache de session se existir
        removeSessionAll();
        //-------------
        clearAlerts();
        if(no_student.prop( "checked" )===true)
        {
            forward_search.hide();
            number_registration.removeClass("required").prop("required", false);
            forward.fadeIn().click();
        }
        else
        {
            number_registration.addClass("required").prop("required", true);
            if(number_registration.val()!=='')
            {
                loading_person.fadeIn();
                removeSessionAll();
                $.ajax({
                    url: "{{ route("Home.Main.validationNumberRegistrationInscribes") }}/" + number_registration.val() + '/' + "{{ isset($model['id']) ? $model['id'] : '' }}",
                    type:"GET",
                })
                    .done(function (response) {

                    })
                    .fail(function (fail) {
                        //
                    })
                    .always(function (always) {
                        //Limpar console
                        //console.clear();
                        validatePerson(always);
                    });
            }
            else
            {
                error_number_registration_null.fadeIn();
            }
        }
    }

    function validateInscribe(always) {
        var inscribeReturn = always.inscribe;
        if(inscribeReturn!==undefined)
        {
            if(inscribeReturn===null)
            {
                inscribe_error.hide();
                loading_inscribe.hide();
                inscribe_success.fadeIn();
                forward_search.hide();
                forward.show().click();
            }
            else
            {
                var inscribe = JSON.stringify(always.inscribe);
                window.sessionStorage.setItem('inscribe', inscribe);
                loading_inscribe.hide();
                inscribe_error.fadeIn();
            }
        }
        else
        {

        }
    }

    function validatePerson(always) {
        var personReturn = always.person;
        if(personReturn!==undefined && personReturn.status==='success')
        {
            clearDataInputsPerson();
            validateInscribe(always);
            var person = JSON.stringify(personReturn);
            window.sessionStorage.setItem('person', person);
            loading_person.hide();
            person_success.fadeIn();
            setDataInputsPerson(getSessionPerson());
        }
        else
        {
            loading_person.hide();
            person_error.fadeIn();
        }
    }

    function setDataInputsPerson(person) {
        for($i=0;$i<person.registrations.length;$i++)
        {
            if(person.registrations[$i].number_registration===number_registration.val())
            {
                var registrationID = person.registrations[$i].id;
            }
        }
        registration_id.val(registrationID);
        person_id.val(person.id);
        first_name.prop("readonly", true).val(person.first_name);
        last_name.prop("readonly", true).val(person.last_name);
        email.prop("readonly", true).val(person.email);
        cpf.prop("readonly", true).val(person.cpf.substring(0,3)+'.'+person.cpf.substring(3,6)+'.'+person.cpf.substring(6,9)+'.'+person.cpf.substring(9,11));
        birth.prop("readonly", true).val(person.birth.substring(8,10)+'/'+person.birth.substring(5,7)+'/'+person.birth.substring(0,4));
        cell_phone.prop("readonly", true).val('(' + person.phones[0].code_area + ')' + person.phones[0].number_phone);
    }

    function clearDataInputsPerson() {
        registration_id.val("").text("");
        person_id.val("").text("");
        first_name.prop("readonly", false).val("").text("");
        last_name.prop("readonly", false).val("").text("");
        email.prop("readonly", false).val("").text("");
        cpf.prop("readonly", false).val("").text("");
        birth.prop("readonly", false).val("").text("");
        cell_phone.prop("readonly", false).val("").text("");
    }

</script>

<script>

    //Verificação por email

    function validateInscribeEmail(always) {
        var inscribeReturn = always.inscribe;
        if(inscribeReturn!==undefined)
        {
            if(inscribeReturn===null)
            {
                inscribe_email_error.hide();
                loading_email_inscribe.hide();

                var valInsErrorEmail  = inscribe_email_error.is(':visible');
                var valInsErrorCPF  = inscribe_cpf_error.is(':visible');
                if (valInsErrorEmail===false && valInsErrorCPF===false){forward.prop("disabled", false);}

                inscribe_email_success.fadeIn();
                return inscribeReturn;
            }
            else
            {
                var inscribe = JSON.stringify(always.inscribe);
                window.sessionStorage.setItem('inscribe', inscribe);
                loading_email_inscribe.hide();
                forward.prop("disabled", true);
                inscribe_email_success.hide();
                inscribe_email_error.fadeIn();

                return inscribeReturn;
            }
        }
        else
        {
            return inscribeReturn;
        }
    }

    function validatePersonEmail(always) {
        var personReturn = always.person;

        if(personReturn!==undefined && personReturn.status==='success')
        {
            var varInscribe = validateInscribeEmail(always);
            var person = JSON.stringify(personReturn);
            window.sessionStorage.setItem('person', person);
            loading_email.hide();
            person_email_error.hide();
            person_email_success.fadeIn();

            var valInsSuccess  = person_email_error.is(':visible');
            if (valInsSuccess===true){inscribe_email_success.hide();}
            else if(varInscribe===null && valInsSuccess!==true){clearDataInputsPersonEmail();email_data_use.show();email_data_clear.show();}

            email_data_use.click(function () {
                event.preventDefault();
                setDataInputsPersonEmail(getSessionPerson());
            });

        }
        else
        {
            loading_email.hide();
            person_email_success.hide();
            person_email_error.hide();

            var valInsError  = inscribe_email_error.is(':visible');
            if (valInsError===true){inscribe_email_error.hide(); inscribe_email_success.hide();}
        }

    }

    function setDataInputsPersonEmail(person) {

        inscribe_cpf_error.hide();
        inscribe_cpf_success.hide();
        person_cpf_success.hide();
        person_cpf_error.hide();
        inscribe_cpf_error.hide();

        for($i=0;$i<person.registrations.length;$i++)
        {
            if(person.registrations[$i].number_registration===number_registration.val())
            {
                var registrationID = person.registrations[$i].id;
            }
        }
        registration_id.val(registrationID);
        person_id.val(person.id);
        first_name.prop("readonly", true).val(person.first_name);
        last_name.prop("readonly", true).val(person.last_name);
        email.prop("readonly", false).val(person.email);
        //var cpfRreplace = person.cpf.replace([3], "*");
        var char3cpf = person.cpf.substring(0, 5)+"******";
        cpf_hidden.show().prop("readonly", true).val(char3cpf);
        cpf.hide().prop("readonly", true).val(person.cpf);
        //-----------------------------------------------------------
        var char3birth = person.birth.substring(0, 5)+"*****";
        birth_hidden.show().prop("readonly", true).val(char3birth);
        birth.hide().prop("readonly", true).val(person.birth);
        //-----------------------------------------------------------
        var char3cell_phone = person.phones[0].number_phone.substring(0, 5)+"*****";
        cell_phone_hidden.show().prop("readonly", true).val(char3cell_phone);
        cell_phone.hide().prop("readonly", true).val('(' + person.phones[0].code_area + ')' + person.phones[0].number_phone);
    }

    email_data_clear.click(function () {
        registration_id.val("").text("");
        person_id.val("").text("");
        first_name.prop("readonly", false).val("").text("");
        last_name.prop("readonly", false).val("").text("");
        email.prop("readonly", false).val("").text("");
        cpf.show().prop("readonly", false).val("").text("");
        birth.show().prop("readonly", false).val("").text("");
        cell_phone.show().prop("readonly", false).val("").text("");
        cpf_hidden.hide().val("").text("");
        birth_hidden.hide().val("").text("");
        cell_phone_hidden.hide().val("").text("");
    });

    email.focusout(function () {
        validateEmail();
    });

    function validateEmail() {
        loading_email.fadeIn();
        //removeSessionAll();
        $.ajax({
            url: "{{ route("Home.Main.validationEmailInscribes") }}/" + email.val() + '/' + "{{ isset($model['id']) ? $model['id'] : '' }}",
            type:"GET",
        })
        .done(function (response) {

        })
        .fail(function (fail) {
            //
        })
        .always(function (always) {
            //Limpar console
            //console.clear();
            validatePersonEmail(always);
        });
    }

    function clearDataInputsPersonEmail() {
        registration_id.val("").text("");
        person_id.val("").text("");
        first_name.prop("readonly", false);
        last_name.prop("readonly", false);
        email.prop("readonly", false);
        cpf.prop("readonly", false);
        birth.prop("readonly", false);
        cell_phone.prop("readonly", false);
    }

</script>

<script>

    //Verificação por cpf

    function validateInscribeCPF(always) {
        var inscribeReturn = always.inscribe;
        if(inscribeReturn!==undefined)
        {
            if(inscribeReturn===null)
            {
                inscribe_cpf_error.hide();
                loading_cpf_inscribe.hide();

                var valInsErrorEmail  = inscribe_email_error.is(':visible');
                var valInsErrorCPF  = inscribe_cpf_error.is(':visible');
                if (valInsErrorEmail===false && valInsErrorCPF===false){forward.prop("disabled", false);}

                inscribe_cpf_success.fadeIn();

                return inscribeReturn;
            }
            else
            {
                var inscribe = JSON.stringify(always.inscribe);
                window.sessionStorage.setItem('inscribe', inscribe);
                loading_cpf_inscribe.hide();
                forward.prop("disabled", true);
                inscribe_cpf_error.fadeIn();

                return inscribeReturn;
            }
        }
        else
        {
            return inscribeReturn;
        }
    }

    function validatePersonCPF(always) {
        var personReturn = always.person;

        var valInsErrorEmail  = inscribe_email_error.is(':visible');
        var valInsErrorCPF  = inscribe_cpf_error.is(':visible');
        if (valInsErrorEmail!==true && valInsErrorCPF!==null){forward.prop("disabled", false);}

        if(personReturn!==undefined && personReturn.status==='success')
        {
            var varInscribe = validateInscribeCPF(always);
            var person = JSON.stringify(personReturn);
            window.sessionStorage.setItem('person', person);
            loading_cpf.hide();
            person_cpf_error.hide();
            person_cpf_success.fadeIn();

            var valInsSuccess  = inscribe_cpf_error.is(':visible');
            if (valInsSuccess===true){inscribe_cpf_success.hide();}
            else if(varInscribe===null && valInsSuccess!==true){clearDataInputsPersonCPF();cpf_data_use.show();cpf_data_clear.show();}

            cpf_data_use.click(function () {
                event.preventDefault();
                setDataInputsPersonCPF(getSessionPerson());
            });
        }
        else
        {
            loading_cpf.hide();
            person_cpf_success.hide();
            person_cpf_error.hide();

            if (valInsErrorCPF===true){inscribe_cpf_error.hide(); inscribe_cpf_success.hide();}
        }
    }

    function testaCPF(strCPF) {
        strCPF = strCPF.replace('.', '').replace('.', '').replace('.', '').replace('-', '');
        var Soma;
        var Resto;
        Soma = 0;
        if (strCPF == "00000000000") return false;

        for (i=1; i<=9; i++) Soma = Soma + parseInt(strCPF.substring(i-1, i)) * (11 - i);
        Resto = (Soma * 10) % 11;

        if ((Resto == 10) || (Resto == 11))  Resto = 0;
        if (Resto != parseInt(strCPF.substring(9, 10)) ) return false;

        Soma = 0;
        for (i = 1; i <= 10; i++) Soma = Soma + parseInt(strCPF.substring(i-1, i)) * (12 - i);
        Resto = (Soma * 10) % 11;

        if ((Resto == 10) || (Resto == 11))  Resto = 0;
        if (Resto != parseInt(strCPF.substring(10, 11) ) ) return false;
        return true;
    }

    function setDataInputsPersonCPF(person) {

        inscribe_email_error.hide();
        inscribe_email_success.hide();
        person_email_success.hide();
        person_email_error.hide();

        for($i=0;$i<person.registrations.length;$i++)
        {
            if(person.registrations[$i].number_registration===number_registration.val())
            {
                var registrationID = person.registrations[$i].id;
            }
        }
        registration_id.val(registrationID);
        person_id.val(person.id);
        first_name.prop("readonly", false).val(person.first_name);
        last_name.prop("readonly", false).val(person.last_name);
        email.prop("readonly", false).val(person.email);
        birth.prop("readonly", false).val(person.birth);
        cell_phone.prop("readonly", false).val('(' + person.phones[0].code_area + ')' + person.phones[0].number_phone);

        var char3email = person.email.substring(0, 5)+"******";
        email_hidden.show().prop("readonly", true).val(char3email);
        email.hide().prop("readonly", true).val(person.email);
        //-----------------------------------------------------------
        cpf.prop("readonly", false).val(person.cpf);
        //-----------------------------------------------------------
        var char3birth = person.birth.substring(0, 5)+"*****";
        birth_hidden.show().prop("readonly", true).val(char3birth);
        birth.hide().prop("readonly", true).val(person.birth);
        //-----------------------------------------------------------
        var char3cell_phone = person.phones[0].number_phone.substring(0, 5)+"*****";
        cell_phone_hidden.show().prop("readonly", true).val(char3cell_phone);
        cell_phone.hide().prop("readonly", true).val('(' + person.phones[0].code_area + ')' + person.phones[0].number_phone);

    }

    cpf_data_clear.click(function () {
        registration_id.val("").text("");
        person_id.val("").text("");
        first_name.prop("readonly", false).val("").text("");
        last_name.prop("readonly", false).val("").text("");
        email.show().prop("readonly", false).val("").text("");
        cpf.show().prop("readonly", false).val("").text("");
        birth.show().prop("readonly", false).val("").text("");
        cell_phone.show().prop("readonly", false).val("").text("");
        email_hidden.hide().val("").text("");
        cpf_hidden.hide().val("").text("");
        birth_hidden.hide().val("").text("");
        cell_phone_hidden.hide().val("").text("");
    });


    cpf.focusout(function () {
        validateCPF();
    });

    function validateCPF() {
        inscribe_cpf_error.hide();
        inscribe_cpf_success.hide();
        person_cpf_success.hide();
        person_cpf_error.hide();
        loading_cpf.fadeIn();
        //removeSessionAll();
        isValid = testaCPF(cpf.val());
        if (!isValid) {
            cpf_invalidated.fadeIn();
            loading_cpf.hide();
            return false;
        }else{
            cpf_invalidated.hide();
        }

        $.ajax({
            url: "{{ route("Home.Main.validationCPFInscribes") }}/" + cpf.val() + '/' + "{{ isset($model['id']) ? $model['id'] : '' }}",
            type:"GET",
        })
        .done(function (response) {

        })
        .fail(function (fail) {
            //
        })
        .always(function (always) {
            //Limpar console
            //console.clear();
            validatePersonCPF(always);
        });
    }

    function clearDataInputsPersonCPF() {
        registration_id.val("").text("");
        person_id.val("").text("");
        first_name.prop("readonly", false);
        last_name.prop("readonly", false);
        email.prop("readonly", false);
        cpf.prop("readonly", false);
        birth.prop("readonly", false);
        cell_phone.prop("readonly", false);
    }

</script>

<script>

    registration_type.change(function () {
        if($(this).val()==='')
        {
            registration_type.addClass('required');
        }
        else
        {
            registration_type.removeClass('required');
        }
    });

    professional_category.change(function () {
        if($(this).val()!=='')
        {
            professional_category.addClass('required');
        }
        else
        {
            professional_category.removeClass('required');
        }
    });

    $('.nice-select').focusout(function () {
        let textoSelectedSpan = $(this).find('.current').text();
        let textoSelectedOption = $(this).closest('.styled-select').find('select option').eq(0).text();
        if (textoSelectedSpan != textoSelectedOption) {
            $(this).closest('.styled-select').find('span.error').remove();
            $(this).closest('.styled-select').find('select').removeClass('required');
        }else{
            $(this).closest('.styled-select').find('select').addClass('required');
        }
    });

</script>


<script>
    button_submit.click(function () {
        let checked = $('#boleto').prop('checked');
        console.log(checked);
        if (checked) {
            loading_payment.fadeIn();
        }
    });
</script>


<script>
    search_2.click(function () {
        removeSessionAll();
        if(search_cpf.val()!=='')
        {
            $.ajax({
                url: "{{ route("Home.Main.validationCPFInscribes") }}/" + search_cpf.val() + '/' + "{{ isset($model['id']) ? $model['id'] : '' }}",
                type:"GET",
            })
            .done(function (response) {
                //
            })
            .fail(function (fail) {
                //
            })
            .always(function (always) {
                var person_name = always.person.data.first_name;
                if(always.inscribe===null)
                {
                    var ins = 'ainda não se inscreveu neste evento! Boa semana para você!';
                }
                else
                {
                    var ins = 'se inscreveu com sucesso! Abaixo as informações da inscrição!';
                    infor_inscribe.text
                    (
                        "Inscrição"
                    );

                    var pay_infor = always.inscribe.payments[0];
                    console.log(pay_infor);
                    if(pay_infor!==null)
                    {
                        infor_payment.append("Código de Pagamento: " + pay_infor.code + ' | <a href="' + pay_infor.link_boleto + '">Imprima o Boleto</a>');
                    }
                }
                infor_person.text("Olá " + person_name + ", você " + ins);
            });
        }
        else
        {
            $.ajax({
                url: "{{ route("Home.Main.validationNumberRegistrationInscribes") }}/" + search_number_registration.val() + '/' + "{{ isset($model['id']) ? $model['id'] : '' }}",
                type:"GET",
            })
                .done(function (response) {
                    //
                })
                .fail(function (fail) {
                    //
                })
                .always(function (always) {
                    var person_name = always.person.data.first_name;
                    if(always.inscribe===null)
                    {
                        var ins = 'ainda não se inscreveu neste evento! Boa semana para você!';
                    }
                    else
                    {
                        var ins = 'se inscreveu com sucesso! Abaixo as informações da inscrição!';
                        infor_inscribe.text
                        (
                            "Inscrição"
                        );

                        var pay_infor = always.inscribe.payments[0];
                        console.log(pay_infor);
                        if(pay_infor!==null)
                        {
                            infor_payment.append("Código de Pagamento: " + pay_infor.code + ' | <a href="' + pay_infor.link_boleto + '">Imprima o Boleto</a>');
                        }
                    }
                    infor_person.text("Olá " + person_name + ", você " + ins);
                });
        }
    });
</script>
