@extends("$route[0].Template.index")

@section('content')
    <main id="general_page">
        <div class="container margin_60">
            <div class="main_title">
                <h2><em></em>{{ $cms_page->session_3_title ? $cms_page->session_3_title : 'Sem Título' }}</h2>
                <p>
                    {{ $cms_page->session_3_subtitle ? $cms_page->session_3_subtitle : 'Sem Conteúdo' }}
                </p>
            </div>
            <!--Team Carousel -->
            <div class="row">
                <div class="owl-carousel owl-theme team-carousel">

                    @foreach($model as $event)

                        <div class="team-item">
                            <div class="team-item-img">
                                <i style="display: none;">{{ $img = $event['img'] }}</i>
                                <img src="{{ $img!=null && $img!='default' ? env('PATH_URL_EUNIG') . "PanelP/img/Events/$img" : env('PATH_URL_EUNIG') . "PanelP/img/Events/default.png" }}" alt="">
                                <div class="team-item-detail">
                                    <div class="team-item-detail-inner">
                                        <h4>{{ $event['name'] }}</h4>
                                        <p>{!! $event['description'] !!}</p>
                                        <ul class="social">
                                            @if($cms_page->facebook!=null)
                                                <li><a href="{{ $cms_page->facebook }}"><i class="icon-facebook"></i></a></li>
                                            @endif
                                            @if($cms_page->twitter!=null)
                                                <li><a href="{{ $cms_page->twitter }}"><i class="icon-twitter"></i></a></li>
                                            @endif
                                            @if($cms_page->google!=null)
                                                <li><a href="{{ $cms_page->google }}"><i class="icon-google"></i></a></li>
                                            @endif
                                            @if($cms_page->linkedin!=null)
                                                <li><a href="{{ $cms_page->linkedin }}"><i class="icon-linkedin"></i></a></li>
                                            @endif

                                            @if($cms_page->youtube!=null)
                                                <li><a href="{{ $cms_page->youtube }}"><i class="icon-youtube"></i></a></li>
                                            @endif
                                            @if($cms_page->instagram!=null)
                                                <li><a href="{{ $cms_page->instagram }}"><i class="icon-instagram"></i></a></li>
                                            @endif
                                        </ul>
                                        <a href="{{ route("Home.Main.show", [$event['id'], $event['tag']]) }}" class="btn_1 white">Acessar Evento</a>
                                    </div>
                                </div>
                            </div>
                            <div class="team-item-info">
                                <h4>{{ $event['name'] }}</h4>
                                <p>{{ $event['code'] }}</p>
                            </div>
                        </div>
                        <!-- /team-item -->

                    @endforeach

                </div>
            </div>
            <!--End Team Carousel-->
        </div>
        <!-- End container -->
        <div class="container_styled_1">
            <div class="container margin_60_35">
                <div class="row">
                    <div class="col-lg-6">
                        <h2 class="nomargin_top">{{ $cms_page->session_2_title ? $cms_page->session_2_title : 'Sem Título' }}</h2>
                        <p>{!! $cms_page->session_2_content ? $cms_page->session_2_content : 'Sem Conteúdo' !!}</p>
                    </div>
                    <div class="col-lg-5 ml-lg-5 add_top_30">
                        <img src="{{ $cms_page->session_2_img_right!=null && $cms_page->session_2_img_right!='default' ? asset("HomeP/img/CmsPages/$cms_page->session_2_img_right") : asset('HomeP/img/devices_2.png') }}" alt="" class="img-fluid">
                    </div>
                </div>
                <!-- End row -->
            </div>
        </div>
    </main>
@endsection
