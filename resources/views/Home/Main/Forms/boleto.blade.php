<!--Tag id="wrapped" retirada -->
<form>
    <div id="middle-wizard">
        <div class="step">
            <h3 class="main_question">Boleto : {{ $model['name'] ? $model['name'] : 'Sem título' }}</h3>
            <a href="{{ session('boleto') ? session('boleto')->link_boleto : 'Não existe' }}"><img src="{{ asset('HomeP/img/boleto.png') }}" class="img-fluid"></a>
            <p>Código de Pagamento: {{ session('boleto') ? session('boleto')->code : 'Não existe' }}</p>
            <p>*será adicionado ao valor total a taxa de R$1,00 da emissão do boleto bancário</p>
        </div>
    </div>
    <div id="bottom-wizard">
        <a href="{{ session('boleto') ? session('boleto')->link_boleto : 'Não existe' }}" id="forward_boleto" class="forward_boleto" target="_blank">Imprimir Boleto <i class="fa fa-check"></i></a>
    </div>
</form>
