<!--Tag id="wrapped" retirada -->
<form>
    <div id="middle-wizard">
        <div class="step">
            <h3 class="main_question">Consulta Para Evento {{ $model['name'] }}</h3>

            <p>Digite seu CPF ou Matrícula para consultar inscrição!</p>

            <div class="col-12">
                <div class="form-group">
                    <input type="text" id="search_cpf" name="cpf" value="" class="col-12 form-control cpf" placeholder="Digite o CPF...">
                </div>
            </div>

            <div class="col-12">
                <div class="form-group">
                    <input type="text" id="search_number_registration" name="number_registration" value="" class="col-12 form-control" placeholder="Digite a Matrícula...">
                </div>
            </div>

            <hr>

            <p class="infor_person"></p>
            <p class="infor_inscribe"></p>
            <p class="infor_payment"></p>

        </div>
    </div>
    <div id="bottom-wizard">
        <button type="button" id="search_2" class="forward_error">Consultar</button>
    </div>
</form>
