<!--Tag id="wrapped" retirada -->
<form method="post" action="{{route("Home.Main.store")}}" enctype="multipart/form-data">
    <input id="website" name="website" type="text" value="">
    <input name="event_id" type="hidden" value="{{ $model['id'] }}">
    <input name="event_name" type="hidden" value="{{ $model['name'] }}">
    <input id="registration_id" name="registration_id" type="hidden" value="">
    <input id="person_id" name="person_id" type="hidden" value="">
    <input id="hashInput" name="hashInput" class="hashInput" type="hidden" value="">
    {{ csrf_field() }}
        <div id="middle-wizard">
        @forelse($steps as $item => $step)
            <i style="display: none;">{{ $item + 1 == count($steps) ? $submit = 'submit' : $submit = '' }}</i>
            <div class="step {{ $submit }}">
                <h3 class="main_question"><strong> {{ $item+1 }} / {{ count($steps) }} </strong>{{ $step['name'] }}</h3>
                @forelse($step['fields'] as $field)

                    @if($field['type']=='text' || $field['type']=='number' || $field['type']=='date' || $field['type']=='email' || $field['type']=='url')
                        <div class="form-group">
                            <input id="{{ $field['tag_id'] }}" {{ $field['required']==1 ? 'required' : '' }} type="{{ $field['type'] }}" name="{{ $field['name']['name'] }}" {{ $field['tags_extras'] }} class="{{ $field['class'] }} {{ $field['required']==1 ? 'required' : '' }} col-{{ $field['cols'] }} form-control" placeholder="{{ $field['placeholder'] }}" style="margin-bottom: 10px;">
                            @if($item==0)
                                <div class="error_number_registration_null" style="margin-bottom: 10px;">
                                    <span style="color: darkred;"><img src="{{ asset('HomeP/img/icon-error.png') }}" style="width: 20px;height: 20px;margin-bottom: 10px;"> | Digite o número de matrícula se for aluno!!</span>
                                </div>
                                <div id="loading_person" style="margin-bottom: 10px;">
                                    <span style="color: darkblue;"><img src="{{ asset('HomeP/img/loading.gif') }}" style="width: 30px;height: 30px;margin-bottom: 10px;"> | Verificando os dados no sistema...</span>
                                </div>
                                <div id="person_success" style="margin-bottom: 10px;">
                                    <span style="color: darkgreen;"><img src="{{ asset('HomeP/img/icon-success.png') }}" style="width: 20px;height: 20px;margin-bottom: 10px;"> | Dados encontrados!</span>
                                </div>
                                <div id="person_error" style="margin-bottom: 10px;">
                                    <span style="color: darkred;"><img src="{{ asset('HomeP/img/icon-error.png') }}" style="width: 20px;height: 20px;margin-bottom: 10px;"> | Dados não Encontrado!</span>
                                </div>

                                <div id="loading_inscribe" style="margin-bottom: 10px;">
                                    <span style="color: darkblue;"><img src="{{ asset('HomeP/img/loading.gif') }}" style="width: 30px;height: 30px;margin-bottom: 10px;"> | Verificando inscrição existente...</span>
                                </div>
                                <div id="inscribe_success" style="margin-bottom: 10px;">
                                    <span style="color: darkgreen;"><img src="{{ asset('HomeP/img/icon-success.png') }}" style="width: 20px;height: 20px;margin-bottom: 10px;"> | Prossiga a inscrição!</span>
                                </div>
                                <div id="inscribe_error" style="margin-bottom: 10px;">
                                    <span style="color: darkred;"><img src="{{ asset('HomeP/img/icon-error.png') }}" style="width: 20px;height: 20px;margin-bottom: 10px;"> | Matrícula já Inscrita, <a href="#">clique aqui</a>, para ver os dados!</span>
                                </div>
                            @endif
                            @if($field['name']['name']=='email')
                                <input id="email_hidden" type="text" value="" class="col-12 form-control" placeholder="Digite o E-mail...">
                                <div id="loading_email" style="margin-bottom: 10px;">
                                    <span style="color: darkblue;"><img src="{{ asset('HomeP/img/loading.gif') }}" style="width: 30px;height: 30px;margin-bottom: 10px;"> | Verificando inscrição existente...</span>
                                </div>
                                <div id="person_email_success" style="margin-bottom: 10px;">
                                    <span style="color: darkgreen;"><img src="{{ asset('HomeP/img/icon-success.png') }}" style="width: 20px;height: 20px;margin-bottom: 10px;"> | Dados encontrados! | <a href="#" id="email_data_use">Usar Esses Dados?</a> | <a style="color: red !important;" href="#" id="email_data_clear" class="fa fa-trash"></a></span>
                                </div>
                                <div id="loading_email_inscribe" style="margin-bottom: 10px;">
                                    <span style="color: darkblue;"><img src="{{ asset('HomeP/img/loading.gif') }}" style="width: 30px;height: 30px;margin-bottom: 10px;"> | Verificando inscrição existente...</span>
                                </div>
                                <div id="inscribe_email_success" style="margin-bottom: 10px;">
                                    <span style="color: darkgreen;"><img src="{{ asset('HomeP/img/icon-success.png') }}" style="width: 20px;height: 20px;margin-bottom: 10px;"> | Prossiga a inscrição!</span>
                                </div>
                                <div id="inscribe_email_error" style="margin-bottom: 10px;">
                                    <span style="color: darkred;"><img src="{{ asset('HomeP/img/icon-error.png') }}" style="width: 20px;height: 20px;margin-bottom: 10px;"> | E-mail já inscrito, <a href="#">clique aqui</a>, para ver os dados!</span>
                                </div>
                            @endif
                            @if($field['name']['name']=='cpf')
                                <input id="cpf_hidden" type="text" value="" class="col-12 form-control" placeholder="Digite o CPF...">
                                <div id="loading_cpf" style="margin-bottom: 10px;">
                                    <span style="color: darkblue;"><img src="{{ asset('HomeP/img/loading.gif') }}" style="width: 30px;height: 30px;margin-bottom: 10px;"> | Verificando inscrição existente...</span>
                                </div>
                                <div id="person_cpf_success" style="margin-bottom: 10px;">
                                    <span style="color: darkgreen;"><img src="{{ asset('HomeP/img/icon-success.png') }}" style="width: 20px;height: 20px;margin-bottom: 10px;"> | Dados encontrados! | <a href="#" id="cpf_data_use">Usar Esses Dados?</a> | <a style="color: red !important;" href="#" id="cpf_data_clear" class="fa fa-trash"></a></span>
                                </div>
                                <div id="loading_cpf_inscribe" style="margin-bottom: 10px;">
                                    <span style="color: darkblue;"><img src="{{ asset('HomeP/img/loading.gif') }}" style="width: 30px;height: 30px;margin-bottom: 10px;"> | Verificando inscrição existente...</span>
                                </div>
                                <div id="inscribe_cpf_success" style="margin-bottom: 10px;">
                                    <span style="color: darkgreen;"><img src="{{ asset('HomeP/img/icon-success.png') }}" style="width: 20px;height: 20px;margin-bottom: 10px;"> | Prossiga a inscrição!</span>
                                </div>
                                <div id="inscribe_cpf_error" style="margin-bottom: 10px;">
                                    <span style="color: darkred;"><img src="{{ asset('HomeP/img/icon-error.png') }}" style="width: 20px;height: 20px;margin-bottom: 10px;"> | CPF já inscrito, <a href="#">clique aqui</a>, para ver os dados!</span>
                                </div>
                                <div id="cpf_invalidated" style="margin-bottom: 10px;">
                                    <span style="color: darkred;"><img src="{{ asset('HomeP/img/icon-error.png') }}" style="width: 20px;height: 20px;margin-bottom: 10px;"> | CPF inválido!</span>
                                </div>
                            @endif
                            @if($field['name']['name']=='birth')
                                <input id="birth_hidden" type="text" value="" class="col-12 form-control" placeholder="Data de Nascimento">
                            @endif
                            @if($field['name']['name']=='cell_phone')
                                <input id="cell_phone_hidden" type="text" value="" class="col-12 form-control" placeholder="(xx)xxxxx-xxxx">
                            @endif
                        </div>
                    @elseif($field['type']=='checkbox' && $field['name']['name']=='policy_terms')
                        <div class="form-group terms">
                            <label class="container_check">{{ $field['placeholder'] }}
                                <a href="#" data-toggle="modal" data-target="#policy-txt">Política de Privacidade</a> e
                                <a href="#" data-toggle="modal" data-target="#terms-txt"> Termos de Serviço</a>
                                <input id="{{ $field['tag_id'] }}" type="checkbox" name="policy_terms"  class="{{ $field['required']==1 ? 'required' : '' }} policy_fadeout {{ $field['class'] }}" {{ $field['tags_extras'] }}>
                                <span class="checkmark"></span>
                            </label>
                        </div>
                    @elseif($field['type']=='checkbox' && $field['name']['name']=='update_account')
                        <div class="form-group terms">
                            <label class="container_check">{{ $field['placeholder'] }}
                                <input id="{{ $field['tag_id'] }}" type="checkbox" name="update_account" {{ $field['required']==1 ? 'checked' : '' }} class="{{ $field['required']==1 ? 'required' : '' }} {{ $field['class'] }}" {{ $field['tags_extras'] }}>
                                <span class="checkmark"></span>
                            </label>
                        </div>
                    @elseif($field['type']=='checkbox' && $field['name']['name']=='newsletter')
                        <div class="form-group terms">
                            <label class="container_check">{{ $field['placeholder'] }}
                                <input id="{{ $field['tag_id'] }}" type="checkbox" name="newsletter" {{ $field['required']==1 ? 'checked' : '' }} class="{{ $field['required']==1 ? 'required' : '' }} {{ $field['class'] }}" {{ $field['tags_extras'] }}>
                                <span class="checkmark"></span>
                            </label>
                        </div>
                    @elseif($field['type']=='checkbox' && ($field['name']['name']!='policy_terms' && $field['name']['name']!='update_account' && $field['name']['name']!='newsletter'))
                        <div class="form-group terms">
                            <label class="container_check">{{ $field['placeholder'] }}
                                <input id="{{ $field['tag_id'] }}" type="checkbox" name="{{ $field['name']['name'] }}" {{ $field['required']==1 ? 'required' : '' }} class="{{ $field['required']==1 ? 'required' : '' }}" {{ $field['tags_extras'] }}>
                                <span class="checkmark"></span>
                            </label>
                        </div>
                    @elseif($field['type']=='select' && $field['select_dinamic']!='static')
                        <div class="form-group">
                            <div class="styled-select clearfix">
                                <select id="{{ $field['tag_id'] }}" class="wide {{ $field['required']==1 ? 'required' : '' }} {{ $field['class'] }}" {{ $field['required']==1 ? 'required' : '' }} name="{{ $field['name']['name'] }}" {{ $field['tags_extras'] }}>
                                    <option value="" selected>{{ $field['placeholder'] }}</option>
                                    @if($field['select_dinamic']=='courses')
                                        @foreach($courses as $course)
                                            <option value="{{ $course['id'] }}" {{ $field['tags_extras'] }}>{{ $course['name'] }} | {{ $course['unity']['corporate_name'] }} | {{ $course['shift']['name'] }} | {{ $course['modality'] }} | {{ $course['category']['name'] }}</option>
                                        @endforeach
                                    @elseif($field['select_dinamic']=='courses')
                                        @foreach($courses as $course)
                                            <option value="{{ $course['id'] }}" {{ $field['tags_extras'] }}>{{ $course['name'] }} | {{ $course['unity']['corporate_name'] }} | {{ $course['shift']['name'] }} | {{ $course['modality'] }} | {{ $course['category']['name'] }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                    @elseif($field['type']=='select' && $field['select_dinamic']=='static')
                        <div class="form-group">
                            <div class="styled-select clearfix">
                                <select id="{{ $field['tag_id'] }}" class="wide {{ $field['required']==1 ? 'required' : '' }} {{ $field['class'] }}" name="{{ $field['name']['name'] }}" {{ $field['tags_extras'] }}>
                                    <option value="" selected>{{ $field['placeholder'] }}</option>
                                    @foreach($field['options'] as $option)
                                        <option value="{{ $option['value'] }}">{{ $option['title'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    @elseif($field['type']=='radio')
                        <div class="col-{{ $field['cols'] }}">
                            <div class="form-group radio_input">
                                <label class="container_radio">{{ $field['placeholder'] }}
                                    <input id="{{ $field['tag_id'] }}" type="{{ $field['type'] }}" name="{{ $field['name']['name'] }}" value="{{ $field['value'] }}" class="{{ $field['required']==1 ? 'required' : '' }}  {{ $field['class'] }}" {{ $field['required']==1 ? 'required' : '' }} {{ $field['tags_extras'] }}>
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                    @endif

                    <div class="loading_payment" style="margin-bottom: 10px;">
                        <span style="color: darkblue;"><img src="{{ asset('HomeP/img/loading.gif') }}" style="width: 30px;height: 30px;margin-bottom: 10px;"> | Gerando Boleto ...</span>
                    </div>

                @empty
                    <div class="form-group">
                        <span>Não existem campos cadastados para esta etapa!</span>
                    </div>
                @endforelse
            </div>
        @empty
            <div class="step submit">
                <h3 class="main_question"><strong>0/0</strong>Não existem etapas para este formulário!</h3>
            </div>
        @endforelse
    </div>
    <div id="bottom-wizard">
        <button type="button" id="backward" name="backward" class="backward"><i class="fa fa-backward"></i> Voltar</button>
        <button type="button" id="reset" class="forward_error">Resetar</button>
        <button type="button" name="forward" id="forward" class="forward">Avançar <i class="fa fa-forward"></i></button>
        <button type="button" id="forward_search" class="forward_error">Avançar <i class="fa fa-check"></i></button>
        <button type="submit" name="process" class="submit submit_finish">Confirmar</button>
    </div>
</form>
