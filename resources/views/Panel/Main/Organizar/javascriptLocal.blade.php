<script>
    $('.alerts_clique').click(function () {
        $('.alerts').hide();
    });
</script>

<script>
    (function(window, document, $, undefined) {
        "use strict";
        $(function() {

            @if(session('success'))
            //== SweetAlert Demo 6
            $(document).ready(function() {
                swal({
                    position: 'center',
                    type: 'success',
                    title: '{{ session('success') }}',
                    text: 'Mensagem irá fechar em 2 segundos.',
                    showConfirmButton: false,
                    timer: 2000
                })
            });
            @endif

            @if(session('error'))
            //== SweetAlert Demo 6
            $(document).ready(function() {
                swal({
                    position: 'center',
                    type: 'error',
                    title: '{{ session('error') }}',
                    text: 'Mensagem irá fechar em 2 segundos.',
                    showConfirmButton: false,
                    timer: 2000
                })
            });
            @endif

        });

    })(window, document, window.jQuery);
</script>


<script>

    $(document).ready(function () {
        //url = "";
        token = $('input[name="_token"]').val();

        $('#event_id').change(function () {
            event_id = $(this).val();
            data = {
                _token: token,
                event_id: event_id
            };
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (response) {
                    //console.log(response[0]);
                    window.localStorage.setItem('response', response[0]);
                    //window.localStorage.removeItem('usuario');
                }
            });
        });
    });


    (function(window, document, $, undefined) {
        "use strict";

        var response = window.localStorage.getItem('response');

        console.log(response);

        $(function() {



            var data7_1 = [
                [0, 4],
                [1, 8],
                [2, 5],
                [3, 10],
                [4, 4],
                [5, 16],
                [6, 5],
                [7, 11],
                [8, 6],
                [9, 11],
                [10, 30]
            ];

            var data7_2 = [
                [0, 1],
                [1, 0],
                [2, 2],
                [3, 0],
                [4, 1],
                [5, 3],
                [6, 1],
                [7, 5],
                [8, 2],
                [9, 3],
                [10, 2]
            ];

            $.plot($("#monthly-budget #monthly-budget-content"),
            [

                {
                    data: data7_1,
                    label: "Nova Iguaçu",
                    points: {
                        show: false
                    },
                    curvedLines: {
                        apply: true
                    },
                    lines: {
                        fill: true
                    }
                },
                {
                    data: data7_2,
                    label: "Itaperuna",
                    points: {
                        show: false
                    },
                    lines: {
                        show: true
                    },
                    yaxis: 2
                }

            ],
                {
                series: {
                    lines: {
                        show: true,
                        fill: true
                    },
                    curvedLines: {
                        apply: true,
                        monotonicFit: true,
                        active: true
                    },
                    points: {
                        show: true,
                        lineWidth: 2,
                        fill: true,
                        fillColor: "#ffffff",
                        symbol: "circle",
                        radius: 5
                    },
                    shadowSize: 0
                },
                grid: {
                    hoverable: true,
                    clickable: true,
                    tickColor: "#e5ebf8",
                    borderWidth: 1,
                    borderColor: "#e5ebf8"
                },
                colors: [QuantumPro.APP_COLORS.accent, QuantumPro.APP_COLORS.primary],
                tooltip: true,
                tooltipOpts: {
                    defaultTheme: false
                },
                xaxis: {
                    ticks: [
                        [1, "Jan"],
                        [2, "Feb"],
                        [3, "Mar"],
                        [4, "Apr"],
                        [5, "May"],
                        [6, "Jun"],
                        [7, "Jul"],
                        [8, "Aug"],
                        [9, "Sep"],
                        [10, "Oct"],
                        [11, "Nov"],
                        [12, "Dec"]
                    ]
                },
                yaxes: [{}, {
                    position: "right" /* left or right */
                }]
            });
        });
    })(window, document, window.jQuery);

</script>
