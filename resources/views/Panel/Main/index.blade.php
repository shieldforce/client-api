@extends("$route[0].Template.index")

@section('content')

    <div class="content-wrapper">

        @includeIf("$route[0].$route[1].index.Header")

        <section class="content">

            <div class="container-fluid">

                <div class="row">

                    @can('Panel.Users.index')
                        @inject('users', '\App\User')
                        <div class="col-lg-3 col-6">
                            <div class="small-box bg-info">
                                <div class="inner">
                                    <h3>{{ $users->count() }}</h3>
                                    <p>Usuários</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-users"></i>
                                </div>
                                <a href="{{ route('Panel.Users.index') }}" class="small-box-footer">Acessar <i class="fas fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                    @endcan

                    @can('Panel.Roles.index')
                        @inject('roles', '\App\Models\Configuration\Roles')
                        <div class="col-lg-3 col-6">
                            <div class="small-box bg-info">
                                <div class="inner">
                                    <h3>{{ $roles->count() }}</h3>
                                    <p>Funções</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-user-check"></i>
                                </div>
                                <a href="{{ route('Panel.Roles.index') }}" class="small-box-footer">Acessar <i class="fas fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                    @endcan

                    @can('Panel.Permissions.index')
                        @inject('permissions', '\App\Models\Configuration\Permissions')
                        <div class="col-lg-3 col-6">
                            <div class="small-box bg-info">
                                <div class="inner">
                                    <h3>{{ $permissions->count() }}</h3>
                                    <p>Permissões</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-lock"></i>
                                </div>
                                <a href="{{ route('Panel.Permissions.index') }}" class="small-box-footer">Acessar <i class="fas fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                    @endcan

                    @can('Panel.Errors.index')
                        @inject('errors', '\App\Models\Configuration\Errors')
                        <div class="col-lg-3 col-6">
                            <div class="small-box bg-info">
                                <div class="inner">
                                    <h3>{{ $errors->count() }}</h3>
                                    <p>Erros</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-bug"></i>
                                </div>
                                <a href="{{ route('Panel.Errors.index') }}" class="small-box-footer">Acessar <i class="fas fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                    @endcan

                </div>

            </div>

        </section>

    </div>

@endsection
