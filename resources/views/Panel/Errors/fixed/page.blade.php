<section class="content">

    <div class="error-page">

        <h2 class="headline text-danger">404</h2>

        <div class="error-content">
            <h3><i class="fas fa-exclamation-triangle text-danger"></i> Oops! Houve uma erro.</h3>

            <p>
                Entre em contato com o E-mail: <a href="mailto:suporteeunig@gmail.com">suporteeunig@gmail.com</a> ou procure este erro em nossa FAQ.
            </p>

            <form class="search-form">
                <div class="input-group">
                    <input type="number" name="search" class="form-control" placeholder="Digite o Código do Erro...">

                    <div class="input-group-append">
                        <button type="submit" name="submit" class="btn btn-danger"><i class="fas fa-search"></i>
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>

</section>