@extends("$route[0].Template.index")

@section('content')
    <div class="content-wrapper">

        @includeIf("$route[0].$route[1].fixed.Header")
        @includeIf("$route[0].$route[1].fixed.page")

    </div>
@endsection