<footer class="main-footer">
    <strong>Copyright &copy; 2014-2019 <a href="httpa://unig.br">Unig Digital</a>.</strong>
    Todos direitos reservados.
    <div class="float-right d-none d-sm-inline-block">
        <b>Version</b> 1.0
    </div>
</footer>
