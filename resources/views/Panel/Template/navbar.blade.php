<nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
        </li>
        <li class="nav-item d-none d-sm-inline-block">
            <a href="{{ route('Panel.Main.index') }}" class="nav-link">Home</a>
        </li>
    </ul>

    <ul class="navbar-nav ml-auto" title="Listas Rápidas">


        @can("Panel.Users.update")
            <li class="nav-item dropdown">
                <a class="nav-link" data-toggle="dropdown" href="#">
                    <i class="fa fa-users-cog"></i>
                    <span class="badge badge-danger navbar-badge">{{ $users_temporary->count() }}</span>
                </a>

                <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">

                    @foreach($users_temporary as $user)

                        <a href="{{ route('Panel.Users.edit', $user->id) }}" class="dropdown-item">
                            <div class="media">
                                <img src="{{ asset('PanelP/dist/img/user1-128x128.jpg') }}" alt="User Avatar" class="img-size-50 mr-3 img-circle">
                                <div class="media-body">
                                    <h3 class="dropdown-item-title">
                                        {{ substr($user->name, 0,20) }}...
                                        <span class="float-right text-sm text-danger"><i class="fas fa-star"></i></span>
                                    </h3>
                                    <p class="text-sm">Usuário Temporário...</p>
                                    <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> {{ $user->created_at->diffForHumans() }}</p>
                                </div>
                            </div>
                        </a>

                        <div class="dropdown-divider"></div>

                    @endforeach

                    <a href="{{ route('Panel.Users.index') }}" class="dropdown-item dropdown-footer">Todos os Usuários</a>

                </div>
            </li>
        @endcan


        <li class="nav-item dropdown user-menu">
            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">
                <img src="{{ auth()->user()->avatar ? asset("PanelP/dist/img/".auth()->user()->avatar) : asset('PanelP/dist/img/user2-160x160.jpg') }}" class="user-image img-circle elevation-2" alt="User Image">
                <span class="d-none d-md-inline">{{ substr(auth()->user()->name, 0, 20) }}...</span>
            </a>
            <ul class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                <!-- User image -->
                <li class="user-header bg-dark">
                    <img src="{{ auth()->user()->avatar ? asset("PanelP/dist/img/".auth()->user()->avatar) : asset('PanelP/dist/img/user2-160x160.jpg') }}" class="img-circle elevation-2" alt="User Image">

                    <p>
                        {{ substr(auth()->user()->name, 0, 20) }}... - {{ substr(auth()->user()->roles[0]->name, 0, 20) }}...
                        <small>Membro {{ auth()->user()->created_at->diffForHumans() }}</small>
                    </p>
                </li>
                <li class="user-footer">
                    <a href="#" class="btn btn-dark btn-flat">Perfil</a>
                    <a href="{{ route('logout') }}" class="btn btn-dark btn-flat float-right" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Sair-S</a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </li>
            </ul>
        </li>



    </ul>
</nav>
