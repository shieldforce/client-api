<a href="#" id="{{ $item->id }}" data-toggle="modal" data-target="#modal-crud{{ $item->id }}" class="btn btn-primary"><i class="fa fa-users-cog"></i></a>
<div class="modal fade" id="modal-crud{{ $item->id }}">
    <div class="modal-dialog">
        <div class="modal-content bg-primary">
            <div class="modal-header">
                <h4 class="modal-title">Usuários do Crud</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Quem Criou: {{ $item->create_user['name'] ?? 'Não Informado' }}!</p>
                <p>Úlmito que Editou: {{ $item->update_user['name'] ?? 'Não Informado' }}!</p>
                <p>Quem Deletou: {{ $item->delete_user['name'] ?? 'Não Informado' }}!</p>
            </div>
            <form>
                {{ csrf_field() }}
                <input type="hidden" name="id" value="{{ $item->id }}"/>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-outline-light" data-dismiss="modal">Fechar</button>
                </div>
            </form>
        </div>
    </div>
</div>
