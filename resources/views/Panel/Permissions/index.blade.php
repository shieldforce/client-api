@extends("$route[0].Template.index")

@section('content')
    <div class="content-wrapper">

        @includeIf("$route[0].$route[1].index.Header")
        @includeIf("$route[0].$route[1].index.Tabela")

    </div>
@endsection