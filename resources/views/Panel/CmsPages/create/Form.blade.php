<section class="page-content container-fluid">
    <div class="row">
        <div class="col">
            <div class="card">
                <h5 class="card-header">Criando {{ $crudName }}</h5>
                <form class="form-horizontal" id="validate-form" method="POST" action="{{ route("$route[0].$route[1].store") }}" enctype="multipart/form-data">

                    {{ csrf_field() }}

                    <div class="card-body">

                    </div>
                    <div class="card-footer bg-light">
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="offset-sm-3 col-md-5">
                                            <button type="submit" class="btn btn-primary btn-rounded submit">Cadastrar</button>
                                            <a href="{{ route("$route[0].$route[1].index") }}" class="btn btn-secondary clear-form btn-rounded btn-outline">Cancelar</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
