<?php

namespace Facebook\WebDriver; //Definição do namespace
use Facebook\WebDriver\Remote\DesiredCapabilities; //chamada à classe de drivers
use Facebook\WebDriver\Remote\RemoteWebDriver; //chamada à classe de WebDriver

header("Content-type: text/html; charset=utf-8"); //definindo o cabeçalho para utf-8

require_once('vendor/autoload.php'); //realizando o autoload das classes pelo composer

$url = 'http://localhost:8000/Home/Tccs/index'; // definindo a url como a do google

$host = 'http://localhost:4444/wd/hub'; // Host default
$capabilities = DesiredCapabilities::chrome(); // escolhendo o driver como chrome
$driver = RemoteWebDriver::create($host, $capabilities, 5000); // criando uma nova conexão com o driver

$driver->get($url); // realizando uma requisição HTTP get na $url

$pesquisa = $driver->findElement(     //método findElement encontra um elemento do html
    WebDriverBy::id('course')     //WebDriverBy::id definimos aqui o id do elemento
)->sendKeys('Ciências Biológicas - Bacharelado'); //método sendKeys "digita" na barra de pesquisa

//$pesquisa->sendKeys(array(WebDriverKeys::ENTER)); //método sendKeys enviando um ENTER na pesquisa

$driver->findElement(     			  //método findElement encontra um elemento do html
    WebDriverBy::id('btn-submit')     //WebDriverBy::id definimos aqui o id do elemento ** doc - https://facebook.github.io/php-webdriver/latest/Facebook/WebDriver/WebDriverBy.html
)->click();							  //método click ** doc - https://facebook.github.io/php-webdriver/latest/Facebook/WebDriver/WebDriverElement.html

?>

<ol>
    <li>Instalar o Java</li>
    <li>Baixar o servidor Selenium (<a target="_blank" href="http://www.seleniumhq.org/download/">http://www.seleniumhq.org/download/</a>), na versão Standalone Server.</li>
    <li>Então, deverá baixar o driver correspondente ao navegador que você gostará de utilizar. Por exemplo, para utilizar o driver do Chrome, você deverá baixar o driver nesta URL.
        (<a target="_blank" href="https://sites.google.com/a/chromium.org/chromedriver/downloads">https://sites.google.com/a/chromium.org/chromedriver/downloads</a>).</li>
    <li>Criar uma variável de ambiente para o driver em Path.</li>
    <li>Startar o selenium iniciando o terminal na pasta do Selenium Stand Alone com o comando java -jar nome_do_arquivo_stand_alone.</li>
</ol>
