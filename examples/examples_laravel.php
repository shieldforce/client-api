<?php

//LARAVEL EXAMPLES---------------------------------------------------------------------------------------------------------

//APLICAÇÃO DE ORDER BY COM GROUP BY
Tcc::orderBy('year','desc')->groupBy('year')->pluck('year');

//WHERERAW PARA UM REPLACE NUM CAMPO DA TABELA
Students::whereRaw("REPLACE(number_registration, '-', '') = ?", str_replace('-', '', $number_registration))->get()->first();

//WHERERAW PARA USO DO CASE WHEN
Tcc::whereRaw("CASE WHEN `number_registration` IS NOT NULL THEN `number_registration` like '%'+'".$request['student_name']."'+'%' ELSE TRUE END");

//WHEREHAS PARA RETORNAR SOMENTE OBJETOS COM RELACIONAMENTO
//PLUCK PARA RETORNAR APENAS UM CAMPO
$course_unitys = $this->courseUnity::where('course_id', 'like', '%'.$request['course'].'%')
        ->where('unity_id', 'like', '%'.$request['unity'].'%')
        ->whereHas('Main')
        ->pluck('id');
