<p align="center"><img src="https://educacaoartetecnologiadotcom.files.wordpress.com/2016/01/educacao-arte-e-tecnologia.png?w=400&h=293&crop=1"></p>

<p align="center">
    Didático Tech
</p>

## Instalação do Repositório Padrão de Módulos Didático Tech

Esse repositório foi construído para ajudar a equipe de desenvolvimento Didático Tech, Aqui estão as instruções de Instalação:

- Abra seu GitBash ou similar na pasta aonde deseja clonar o repositório.
- Digite o seguinte comando no GitBash : [git clone https://github.com/Shieldforce/base-shieldforce.git] .
- Assin que o repositório terminar de baixar dê o próximo comando: [composer update].
- Depois Copie o arquivo .env.example e renomeie para .env. <br>

  DB_CONNECTION=mysql <br>
  DB_HOST=[ip ou nome do servidor] <br>
  DB_PORT=[porta de acesso ao banco do servidor] <br>
  DB_DATABASE=passport <br>
  DB_USERNAME=root <br>
  DB_PASSWORD=passport <br>
  
- Crie seu banco de dados.
- Configure seu arquivo .env com os dados do seu host.
- Gerar a Key com o comando [php artisan key:generate].
- Logo após dê o comando no GitBash : [php artisan migrate].
- E então o comando : [php artisan db:seed].
- Então Crie o Virtual Host.
- Acesso padrão user: admin@admin e pass: admin e mãos a obra.

Pronto o pacote já pode ser usado.

## Agradecimentos

Obrigado a Equipe por colaborarem para o crescimento da empresa.

## Licença

Lisença:  [MIT license](https://opensource.org/licenses/MIT).
